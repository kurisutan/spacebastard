/***************************************************************************************************************************
＜プレイヤー処理＞
2019年07月08日（月曜日）　
***************************************************************************************************************************/

/******************************************
				ヘッダー読み込み
*******************************************/
#include "player.h"
#include "input.h"
#include "common.h"
#include "texture.h"
#include "sprite.h"
#include "SpriteAnim.h"
#include "bullet.h"
#include "specialweapon.h"
#include "debug_font.h"
#include "math.h"

/******************************************
			　グローバル変数
*******************************************/
int Texture_IDShip = 0;				//テクスチャID
int Texture_IDUI = 0;
int Texture_IDUIlow = 0;
int Texture_Texture_R = 0;
int Texture_Texture_G = 0;
int Texture_Texture_B = 0;

static float less = 1;
static float g_player_speed;
static int bulletLimit;
static PLAYER g_player;
static BULLET *p_Bullet;
static SPECIALWEAPON *p_spWeapon;
static D3DXVECTOR2 dir(0.0f, 0.0f);


/************************************************************
*						初期化								*
*************************************************************/
void playerInit()
{
	//Texture_Init();						//テクスチャ初期化
	Texture_IDShip = Texture_SetLoadFile("ship.png", PLAYER_W, PLAYER_H);				//読み込みの準備
	Texture_IDUI = Texture_SetLoadFile("UI.png", 206, 72);				//読み込みの準備
	Texture_IDUIlow = Texture_SetLoadFile("lowUI.png", 181, 61);				//読み込みの準備
	Texture_Texture_R = Texture_SetLoadFile("redbar.png", 103, 9);
	Texture_Texture_G = Texture_SetLoadFile("greenbar.png", 103, 9);
	Texture_Texture_B = Texture_SetLoadFile("bluebar.png", 103, 9);
	Texture_Load();			//テクチャデータ読み込み

	g_player.collision.centerposition = g_player.position;
	g_player.collision.radius = 48;
	g_player.position.x = -2700;	//プレイヤーの初期位置(横)
	g_player.position.y = 2500;	//プレイヤーの初期位置(縦)
	g_player.playerLookAtX = 0.0f;
	g_player.playerLookAtY = 0.0f;
	g_player.facing = 0;
	g_player.radius = 32;
	g_player.vectorx = 15;
	g_player.vectory = 15;
	g_player.shootangle = 0;
	g_player.isPress = false;
	g_player.maxWidth = SCREHN_WIDTH / 2;
	g_player.maxHeight = SCREHN_HEIGHT / 2;
	bulletLimit = 0;
}

/************************************************************
*						終了処理							*
*************************************************************/
void playerUnnit()
{
	Texture_Release();					//テクスチャデータ開放
}

/************************************************************
*							更新						    *
*************************************************************/
void playerUpdate()
{
	SpriteAnim_Update();

	g_player.vectorx *= 0.99f;
	g_player.vectory *= 0.99f;

	g_player.maxWidth  = (g_player.position.x + SCREHN_WIDTH);
	g_player.maxHeight = (g_player.position.y - SCREHN_HEIGHT);

	g_player_speed = 15;
	g_player.collision.centerposition = g_player.position;


	//This is for Shooting Command Controls


	if (Keyboard_IsPress(DIK_W))
	{
		g_player.vectorx = g_player_speed;
		g_player.vectory = g_player_speed;

		//Player_position_Y -= 10.0f;	//上
		//Player_position_Y = max(Player_position_Y, PLAYER_H*0.5f);	//画面上部に行ったとき
	}

	g_player.position.x += g_player.vectorx * cos(g_player.facing);
	g_player.position.y += g_player.vectory * sin(g_player.facing);


	if (Keyboard_IsPress(DIK_S))
	{
		if (dir.y <= 3.0f)
		{
			dir.y += 0.1f;
		}

		//Player_position_Y += 10.0f;	//下
		//Player_position_Y = min(Player_position_Y, SCREHN_HEIGHT - PLAYER_H * 0.5f);	//画面下部に行ったとき
	}
	if (Keyboard_IsPress(DIK_A))
	{
		//かんくん
		g_player.facing -= 0.1f;
	}
	if (Keyboard_IsPress(DIK_D))
	{
		//かんくん
		g_player.facing += 0.1f;
	}

	p_Bullet = GetBullet();
	if (Keyboard_IsPress(DIK_SPACE) && bulletLimit > 30)
	{
		if ((p_Bullet)->bulletType == UPGRADED)
		{
			CreateBullet(g_player.position.x, g_player.position.y, 20,  2, g_player.facing);
			CreateBullet(g_player.position.x, g_player.position.y, 20,  0, g_player.facing);
			CreateBullet(g_player.position.x, g_player.position.y, 20, -2, g_player.facing);
			bulletLimit = 0;
		}
		else if ((p_Bullet)->bulletType == NORMAL)
		{
			CreateBullet(g_player.position.x, g_player.position.y, 20, 0, g_player.facing);
			bulletLimit = 0;
		}
	}

	p_spWeapon = GetSpecialWeapon();
	if (p_spWeapon->type == SPSAW)
	{
		if (Keyboard_IsPress(DIK_B))
		{
			p_spWeapon->isUseSp = true;
		}
		else
		{
			p_spWeapon->isUseSp = false;
		}
	}


	//g_player.facing * 180 / D3DX_PI;


	bulletLimit++;



}

/************************************************************
*							描画								*
*************************************************************/
void playerDrow()
{
	//void Sprite_Draw_Rotation(int textureId, float dx, float dy, int cut_x, int cut_y, int cut_w, int cut_h, float center_x, float center_y, float angle)
			//かんくん
	SpriteAnim_Drow(Texture_IDShip, cameraX(g_player.position.x), cameraY(g_player.position.y), PLAYER_W / 5, PLAYER_H / 2, 64, 96, g_player.facing);
	Sprite_Draw(Texture_IDUI, 100, 10, 206, 72, 206, 72);
	Sprite_Draw(Texture_IDUIlow, 100, 80, 181, 61, 181, 61);
	Sprite_Draw(Texture_Texture_R, 175, 20, 103, 9, 103, 9);
	Sprite_Draw(Texture_Texture_G, 175, 40, 103, 9, 103, 9);
	Sprite_Draw(Texture_Texture_B, 175, 60, 103, 9, 103, 9);
}

CIRCLE GetPlayerCollision()
{
	return g_player.collision;
}

PLAYER* GetPlayer()
{
	return &g_player;
}

