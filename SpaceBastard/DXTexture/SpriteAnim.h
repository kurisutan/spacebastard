//インクルードガード
#ifndef _SPRITEANIM_
#define _SPRITEANIM_

#define COCO_WALK_ANIM_PATTERN_MAX (10)
#define COCO_WALK_ANIM_STOP (7)
#define COCO_WALK_ANIM_H (32)
#define COCO_WALK_ANIM_W (32)

/***************************************
			プロトタイプ宣言
****************************************/
void SpriteAnim_Init(void);
void SpriteAnim_Uninit(void);
void SpriteAnim_Update(void);
void SpriteAnim_Drow(float dx, float dy);
void SpriteAnim_Drow(int TextureID, float dx, float dy, float cut_dx, float cut_dy, float cut_x, float cut_y);
void SpriteAnim_Drow(int TextureID, float dx, float dy, float cut_dx, float cut_dy, float cut_x, float cut_y, float angle);
void SpriteAnim_Drowsp(int TextureID, float dx, float dy, float cut_dx, float cut_dy, float cut_x, float cut_y, int angle);
void SpriteAnim_DrowFlip(int TextureID, float dx, float dy, float cut_dx, float cut_dy, float cut_x, float cut_y);
void SpriteAnim_DrowExplosion(int ID, float dx, float dy, float cut_x, float cut_y);
//インクルードガード
#endif