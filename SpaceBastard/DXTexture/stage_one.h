#pragma once

struct STAGEONE
{
	int stageoneTimer;
};

void InitStageone();
void UninitStageone();
void UpdateStageone();
void DrawStageone();

STAGEONE *GetStageone();