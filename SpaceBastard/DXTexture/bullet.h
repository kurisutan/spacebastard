#pragma once
#include "Direct3D.h"
#include "collision.h"
//弾のマックス決める
#define MAXBULLET (50)
//弾の速さ
#define BULLETSPEED (20)
/***************************************
			プロトタイプ宣言
****************************************/
enum BULLETTYPE
{
	NORMAL,
	UPGRADED,
	UPGRADEX1,
	UPGRADEX2,
};


struct BULLET
{
	D3DXVECTOR2 position;
	bool isUseBullet;
	int bulletSpeedx;
	int bulletSpeedy;
	int bulletAmount;
	double angle;
	CIRCLE bulletcollision;
	BULLETTYPE bulletType;
};
void bulletInit();		//初期化
void bulletUnnit();		//終了処理
void bulletUpdate();		//更新
void bulletDrow();		//描画
void CreateBullet(float x, float y, float speedX, float speedY, double angle);
CIRCLE GetBulletCollision(int index);
BULLET* GetBullet();