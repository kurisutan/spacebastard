#include "specialweapon.h"
#include "player.h"
#include "texture.h"
#include "sprite.h"
#include "SpriteAnim.h"
#include "collision.h"


SPECIALWEAPON g_spweapon;
PLAYER *l_player;
int Texture_IDSP = 0;
static int angletimer;
static int angle;

void spWeaponInit()
{
	g_spweapon.position.x = 0;
	g_spweapon.position.y = 0;
	g_spweapon.spcollision.centerposition = g_spweapon.position;
	g_spweapon.spcollision.radius = 0;
	g_spweapon.isUseSp = false;
	g_spweapon.type = SPSAW;

	angle = 0;
	angletimer = 0;
	Texture_IDSP = Texture_SetLoadFile("sawww.png", 1280, 256);				//読み込みの準備
	Texture_Load();
}
void spWeaponUninit()
{
	Texture_Release();
}
void spWeaponUpdate()
{
	l_player = GetPlayer();
	g_spweapon.position = l_player->position;

	if (g_spweapon.isUseSp)
	{
		g_spweapon.spcollision.centerposition = g_spweapon.position;

		if (g_spweapon.type == SPSAW)
		{
			g_spweapon.spcollision.radius = 128;
		}
	}
	else if (g_spweapon.isUseSp == false)
	{

	}
}
void spWeaponDraw()
{
	if (g_spweapon.isUseSp)
	{
		angletimer++;
		if (angletimer == 5)
		{
			angle += 45;
			angletimer = 0;
		}
		//SpriteAnim_Drowsp(int TextureID, float dx, float dy, float cut_x, float cut_y, float cut_dx, float cut_dy, int x, int y, int z)
		SpriteAnim_Drowsp(Texture_IDSP, l_player->position.x + 50, l_player->position.y, 256, 256, 256, 256, angle);
	}
}

CIRCLE GetSpCollision()
{
	return g_spweapon.spcollision;
}

SPECIALWEAPON* GetSpecialWeapon()
{
	return &g_spweapon;
}
