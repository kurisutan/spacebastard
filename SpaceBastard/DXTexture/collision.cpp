#include "collision.h"



bool IsHitCollision_Circle_Circle(const CIRCLE* pA, const CIRCLE* pB)
{
	return D3DXVec2LengthSq(&(pB->centerposition - pA->centerposition)) < (pA->radius + pB->radius) * (pA->radius + pB->radius);
}