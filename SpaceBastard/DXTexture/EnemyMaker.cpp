
/***************************************************************************************************************************
＜ゲーム処理＞
2019年07月08日（月曜日）
***************************************************************************************************************************/
#include "player.h"
#include "bullet.h"
#include "enemy.h"
#include "system_timer.h"
#include "game.h"
#include "stage_one.h"
#include "enemy.h"

static int g_FrameCount = 0;
static float enemylocationx;
static float enemylocationy;

MAINMENU *pmenu;
STAGEONE *pstageone;

/************************************************************
*						初期化								*
*************************************************************/
void enemyInit()
{
	EnemyInit();
	enemylocationx = 0;
	enemylocationy = 0;
}

/************************************************************
*						終了処理							*
*************************************************************/
void enemyUnnit()
{
	EnemyUnnit();
}

/************************************************************
*							更新						    *
*************************************************************/
void enemyUpdate()
{
	pmenu = GetScene();
	pstageone = GetStageone();
	EnemyUpdate();
	if (pmenu->scene == SCENE_STAGE1)
	{
		if (pstageone->stageoneTimer < 20)
		{
			switch ((g_FrameCount / 30) % 3)
			{
			case 0:
				enemylocationx = 1980;
				enemylocationy = 200;
				CreateEnemy(0, enemylocationx, enemylocationy);
				break;
			case 1:
				enemylocationx = 1980;
				enemylocationy = 300;
				CreateEnemy(0, enemylocationx, enemylocationy);
				break;
			case 2:
				enemylocationx = 1980;
				enemylocationy = 200;
				CreateEnemy(0, enemylocationx, enemylocationy);
				break;
			}
		}
		//if (pstageone->stageoneTimer > 20 && pstageone->stageoneTimer < 40)
		//{
		//	switch ((g_FrameCount / 30) % 3)
		//	{
		//	case 0:
		//		enemylocationx = 1980;
		//		enemylocationy = 600;
		//		CreateEnemy(0, enemylocationx, enemylocationy);
		//		CreateEnemy(0, enemylocationx, enemylocationy + 100);
		//		break;
		//	case 1:
		//		enemylocationx = 1980;
		//		enemylocationy = 700;
		//		CreateEnemy(0, enemylocationx, enemylocationy);
		//		CreateEnemy(1, enemylocationx, enemylocationy - 100);
		//		break;
		//	case 2:
		//		enemylocationx = 1980;
		//		enemylocationy = 600;
		//		CreateEnemy(0, enemylocationx, enemylocationy);
		//		CreateEnemy(0, enemylocationx, enemylocationy + 100);
		//		break;
		//	}
		//}
		//if (pstageone->stageoneTimer > 40 && pstageone->stageoneTimer < 60)
		//{
		//	switch ((g_FrameCount / 30) % 3)
		//	{
		//	case 0:
		//		enemylocationx = 1980;
		//		enemylocationy = 400;
		//		CreateEnemy(SMALL, enemylocationx - 100, enemylocationy - 50);
		//		CreateEnemy(SMALL, enemylocationx, enemylocationy);
		//		CreateEnemy(SMALL, enemylocationx - 100, enemylocationy + 50);
		//		break;
		//	case 1:
		//		enemylocationx = 1980;
		//		enemylocationy = 500;
		//		CreateEnemy(SMALL, enemylocationx - 100, enemylocationy - 50);
		//		CreateEnemy(SMALL, enemylocationx, enemylocationy);
		//		CreateEnemy(SMALL, enemylocationx - 100, enemylocationy + 50);
		//		break;
		//	case 2:
		//		enemylocationx = 1980;
		//		enemylocationy = 400;
		//		CreateEnemy(SMALL, enemylocationx - 100, enemylocationy - 50);
		//		CreateEnemy(SMALL, enemylocationx, enemylocationy);
		//		CreateEnemy(SMALL, enemylocationx - 100, enemylocationy + 50);
		//		break;
		//	}
		//}
		//if (pstageone->stageoneTimer > 60 && pstageone->stageoneTimer < 80)
		//{
		//	switch ((g_FrameCount / 60) % 5)
		//	{
		//	case 0:
		//		enemylocationx = 1980;
		//		enemylocationy = 400;
		//		CreateEnemy(SMALL, enemylocationx, enemylocationy);
		//		break;
		//	case 1:
		//		enemylocationx = 1980;
		//		enemylocationy = 500;
		//		CreateEnemy(SMALL, enemylocationx, enemylocationy);
		//		break;
		//	case 2:
		//		enemylocationx = 1980;
		//		enemylocationy = 400;
		//		CreateEnemy(SMALL, enemylocationx, enemylocationy);
		//		break;
		//	}
		//}
	}
	g_FrameCount++;
}

/************************************************************
*							描画							*
*************************************************************/
void enemyDrow()
{

}