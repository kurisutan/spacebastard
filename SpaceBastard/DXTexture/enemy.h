#pragma once
#include "Direct3D.h"
#include "collision.h"
#define MAXENEMY (100)

#define ENEMYL_W (64)
#define ENEMYL_H (32)

#define ENEMYM_W (128)
#define ENEMYM_H (32)

#define ENEMYS_W (64)
#define ENEMYS_H (32)
/***************************************
			プロトタイプ宣言
****************************************/
enum ENEMYTYPE
{
	SMALL,
	MEDIUM,
	LARGE,
};
struct ENEMY
{
	D3DXVECTOR2 position;
	float enemyCurveX;
	float enemyCurveY;
	float enemySpeed;
	float enemyTimer;
	bool isUseEnemy;
	int basicenemyHP;
	int enemydeathCount;
	CIRCLE collision;
	ENEMYTYPE enemyType;
};
void EnemyInit();		//初期化
void EnemyUnnit();		//終了処理
void EnemyUpdate();		//更新
void EnemyDrow();		//描画
void CreateEnemy(int type, float x, float y);
void resetEnemy();
CIRCLE GetEnemyCollision(int index);
ENEMY* GetEnemy();