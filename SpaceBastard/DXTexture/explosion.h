#ifndef _EXPLOSION_H_
#define _EXPLOSION_H_

#define MAXEXPLOSION (50)

struct EXPLOSION
{
	D3DXVECTOR2 position;
	bool isUseExplosion;
	float ExplosionTimer;
};

void explosionInit(void);
void explosionUninit(void);
void explosionUpdate(void);
void explosionDrow(void);
void CreateExplosion(float x, float y);
int GetFrameCounterZero();


#endif _EXPLOSION_H_
