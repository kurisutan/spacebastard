#pragma once
#include "Direct3D.h"
#include "collision.h"

#define PLAYER_START_X (0.0f)		//プレイヤーの場所(横)
#define PLAYER_START_Y (0.0f)		//プレイヤーの場所(縦)
#define PLAYER_W (320)		//プレイヤーの幅
#define PLAYER_H (192)		//プレイヤーの高さ
#define PLAYER_DEFAULT_SPEED (5)		//プレイヤーの高さ

struct PLAYER
{
	D3DXVECTOR2 position;
	CIRCLE collision;
	int HP;
	int ARMOR;
	int SPGAUGE;
	double facing;
	int radius;
	int vectorx;
	int vectory;
	float maxHeight;
	float maxWidth;
	double shootangle;
	double playerLookAtX;
	double playerLookAtY;

	bool isPress;
};


/***************************************
			プロトタイプ宣言
****************************************/
void playerInit();		//初期化
void playerUnnit();		//終了処理
void playerUpdate();	//更新
void playerDrow();		//描画
CIRCLE GetPlayerCollision();
PLAYER* GetPlayer();