#define _CRT_SECURE_NO_WARNINGS
#include "main.h"
#include "texture.h"
#include"Direct3D.h"

struct TextureDate
{
	char filename[FILENAME_MAX];		//二重読み込み防止
	int width;							//幅
	int hegiht;							//高さ
	LPDIRECT3DTEXTURE9 pTextue;			//テクスチャ
};

static TextureDate g_TextureDate[TEXTUREDATE_MAX];

/************************************************************
*						初期化								*
*************************************************************/
void Texture_Init(void)
{
	for (int i = 0; i < TEXTUREDATE_MAX; i++)
	{
		g_TextureDate[i].filename[0] = 0;			//配列の初期化(一番最初の要素に<0>を代入すると全部初期化される
		g_TextureDate[i].pTextue = NULL;
	}
}

int Texture_SetLoadFile(const char *pFileName, int width, int height)
{
	//すでに登録されていないかの検索をする
	for (int i = 0; i < TEXTUREDATE_MAX; i++)
	{
		if (strcmp(g_TextureDate[i].filename, pFileName) == 0)
		{
			return i;	//整理番号を返す(登録している場所)
		}
	}
	//データベースの使用されていない場所を検索して登録する
	for (int i = 0; i < TEXTUREDATE_MAX; i++)
	{
		if (g_TextureDate[i].filename[0] != 0)	//データベースが使用されている
		{
			continue;	//検索に戻る(ループの最初)
		}
		else			//データベースが使用されていない
		{
			strcpy(g_TextureDate[i].filename, pFileName);	//ファイル名登録(コピー)
			g_TextureDate[i].width = width;					//テクスチャの幅
			g_TextureDate[i].hegiht = height;				//テクスチャの高さ
			return i;	//整理番号を返す(登録している場所)
		}
	}
	return -1;			//登録できる場所がない(エラー)
}

int Texture_Load(void)
{
	int error_count = 0;							//エラー件数格納用
	LPDIRECT3DDEVICE9 My_TextureDevice = NULL;		//デバイス情報を受け取る変数
	My_TextureDevice = MYDirect3D_GetDevice();		//デバイス情報を受け取る

	for (int i = 0; i < TEXTUREDATE_MAX; i++)
	{
		if (g_TextureDate[i].filename[0] == 0)		//ファイルネームがなかったら
		{
			error_count++;
			continue;	//検索に戻る(ループの最初)
		}
		if (g_TextureDate[i].pTextue != NULL)		//テクスチャデバイスを持ってたら
		{
			error_count++;
			continue;	//検索に戻る(ループの最初)
		}

		if (FAILED(D3DXCreateTextureFromFile(My_TextureDevice, g_TextureDate[i].filename, &g_TextureDate[i].pTextue)))	//ファイル読み込みが失敗したら
		{
			MessageBox(NULL, "テクスチャデータの読み込みに失敗しました。ファイルネームを確認して下さい", "Base", MB_OK | MB_ICONSTOP);
			error_count++;
		}
	}
	return error_count;		//エラーになった件数を返す
}

void Texture_Release(int ids[], int count)
{
	for (int i = 0; i < count; i++)		//消したいカウント分
	{
		if (g_TextureDate[ids[i]].pTextue == NULL)									//使われていなかったら
		{
			MessageBox(NULL, "テクスチャデータの解放に失敗しました。ID番号を確認して下さい", "Base", MB_OK | MB_ICONSTOP);
		}

		SAFE_RELEASE(g_TextureDate[ids[i]].pTextue)
			g_TextureDate[ids[i]].filename[0] = 0;

	}
}

void Texture_Release(void)
{
	for (int i = 0; i < TEXTUREDATE_MAX; i++)	//全部消す
	{
		SAFE_RELEASE(g_TextureDate[i].pTextue);
		g_TextureDate[i].filename[0] = 0;
	}
}

LPDIRECT3DTEXTURE9 Texture_GetTexture(int id)
{
	return	g_TextureDate[id].pTextue;
}

int Texture_GetWidth(int id)
{
	return	g_TextureDate[id].width;
}

int Texture_GetHeight(int id)
{
	return	g_TextureDate[id].hegiht;
}