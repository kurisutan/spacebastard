//インクルードガード
#ifndef _TEXTURE_H_
#define _TETXURE_H_

#include<d3d9.h>		//DirectX9
#include<d3dx9.h>	
#include <Windows.h>	

/****************************************
				定数定義
*****************************************/
#define FILENAME_MAX (128)	//8の倍数 テクスチャデータのファイル名の長さ
#define TEXTUREDATE_MAX (256)	//テクスチャをどれだけ読み込むか

/***************************************
			プロトタイプ宣言
****************************************/
void Texture_Init(void);		//テクスチャ管理モジュールの初期化

// ファイル名,テクスチャの画像の幅,テクスチャの画像の高さ　戻り値が予約番号
int Texture_SetLoadFile(const char *pFileName, int width, int height);	//読み込みしたいファイルの登録

//戻り値が読み込みに失敗した数
int Texture_Load(void);		//登録したファイルを読み込む(一気に)

//int型の先頭アドレス,配列の要素数(解放したいデータの数)
void Texture_Release(int ids[], int count);	//情報を部分解放
void Texture_Release(void);		//情報を全解放

//テクスチャのID情報
LPDIRECT3DTEXTURE9 Texture_GetTexture(int id);	//idに合ったデバイス情報をもらう
int Texture_GetWidth(int id);	//幅をもらう
int Texture_GetHeight(int id);	//高さをもらう

//インクルードガード
#endif
