#include "texture.h"
#include "score.h"
#include "sprite.h"

//数字のテクスチャのサイズ
#define NUMBER_WIDTH  256
#define NUMBER_HEIGHT 256
//1文字のサイズ
#define SCORE_WIDTH   16
#define SCORE_HEIGHT  16

SCORE g_score;

//プロトタイプ宣言
static void numberDraw(int number, float x, float y);

static int ScoreTexture = -1;


void Score_Init(void)
{
	ScoreTexture = Texture_SetLoadFile("./rom/number256.tga", NUMBER_WIDTH, NUMBER_HEIGHT);
}



void Score_Uninit(void)
{
	Texture_Release();
}



void Score_Update(void)
{

}



void Score_Draw(int score, float x, float y, int digit, bool bZero, bool bLeft)
{
	for (int i = 0; i < digit; i++)
	{
		int n = score % 10;

		//---------------
		numberDraw(n, x - (SCORE_WIDTH*i), y);
		//---------------


		score /= 10;
		n = score % 10;
	}
}



void numberDraw(int number, float x, float y)
{
	//1ケタの表示範囲
	if (number >= 0 && number <= 9)
	{
		Sprite_Draw(ScoreTexture, x, y, number * SCORE_WIDTH, 0, SCORE_WIDTH, SCORE_HEIGHT);
	}

}


int PlusScore(int point)
{
	g_score.number += point;
	return g_score.number;
}

SCORE *GetScore()
{
	return &g_score;
}