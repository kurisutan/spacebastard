#pragma once

#pragma comment(lib,"d3d9.lib")
#if defined(DEBUG)||defined(_DEBUG)
#pragma comment(lib, "d3dx9d.lib")
#else 
#pragma comment(lib, "d3dx9.lib")
#endif

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

/***************************************
			   インクルード
****************************************/
#include"common.h"
/***************************************
			プロトタイプ宣言
****************************************/
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
void Init(HWND hWnd);//初期化
void Uninit(void);	//終了処理
void Update(void);	//更新
void Draw(void);	//描画

/****************************************
				定数定義
*****************************************/
#define CLASS_NAME "GameWindow"
#define WINDOW_CAPTON "ポリゴン描画"
#define WINDOW_STYLE WS_OVERLAPPEDWINDOW	//ウィンドウのスタイイル設定
#define FPS_MEASUREMENT_TIME (1.0)
/****************************************
				構造体
*****************************************/