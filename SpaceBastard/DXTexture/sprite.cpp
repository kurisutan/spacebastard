#include"common.h"
#include "texture.h"
#include "sprite.h"
#include"Direct3D.h"
#include "player.h"
#include "input.h"

/**************************************
			　グローバル変数
**************************************/
static PLAYER *c_Player = GetPlayer();
static D3DCOLOR g_color = D3DCOLOR_RGBA(255, 255, 255, 255);

static LPDIRECT3DVERTEXBUFFER9 g_pVertexBaffer = NULL;	//頂点バッファのアドレス格納(メモ帳)
static LPDIRECT3DINDEXBUFFER9 g_pIndexBaffer = NULL;	//インデックスバッファのアドレス格納(メモ帳)
static LPDIRECT3DDEVICE9 p_Device = NULL;
/************************************************************
*						初期化								*
*************************************************************/
void Sprite_Init(void)
{
	p_Device = MYDirect3D_GetDevice();
	//メモリの量(頂点の),使い方,FVF(なくても動く),デバイスの復旧方法(デバイス又はじぶんが復旧),頂点バッファのメモ帳,基本<NULL>
	p_Device->CreateVertexBuffer(sizeof(Vertex2d) * 4, D3DUSAGE_WRITEONLY, FVF_VERTEX2D, D3DPOOL_MANAGED, &g_pVertexBaffer, NULL);	//比較的失敗することはない
	//メモリの量(頂点の),使い方,仕様,デバイスの復旧方法(デバイス又はじぶんが復旧),頂点バッファのメモ帳,基本<NULL>
	p_Device->CreateIndexBuffer(sizeof(WORD) * 6, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &g_pIndexBaffer, NULL);	//インデックスバッファ
	WORD *pIndex;
	g_pIndexBaffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);	//<VRAM>への施錠
	//インデックスを格納
	pIndex[0] = 0;
	pIndex[1] = 1;
	pIndex[2] = 2;
	pIndex[3] = 1;
	pIndex[4] = 3;
	pIndex[5] = 2;
	g_pIndexBaffer->Unlock();	//解錠

	p_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	p_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	p_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	for (int i = 0; i < 10; i++)
	{
		p_Device->SetTextureStageState(i, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
		p_Device->SetTextureStageState(i, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		p_Device->SetTextureStageState(i, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
	}
}

/************************************************************
*						終了処理							*
*************************************************************/
void Sprite_Uninit(void)
{
	SAFE_RELEASE(g_pVertexBaffer);
	SAFE_RELEASE(g_pIndexBaffer);
}

/************************************************************
*						回転(傾き)							*
*************************************************************/

void Sprite_Draw(int textureId, float dx, float dy, int cut_x, int cut_y, int cut_w, int cut_h)
{
	//テクスチャの幅、高さ取得
	int w = Texture_GetWidth(textureId);
	int h = Texture_GetHeight(textureId);

	//左上と右下のuvの計算
	float u0 = cut_x / (float)w;               //全体に対するカットしたいとこのu座標(左上)
	float v0 = cut_y / (float)h;               //v座標
	float u1 = (cut_x + cut_w) / (float)w;     //全体に対するカットしたいとこのu座標(右下)
	float v1 = (cut_y + cut_h) / (float)h;      //v座標

	//頂点情報
	//デバイスにαブレンド有効の設定をする(aブレンド有効化、今有効かどうか)
	p_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	//αブレンドパラメータ
	//今から描くポリゴンのRGB x 今から描くポリゴンのα + 画面のRGB x (1-今から描くポリゴンのα) ※今回255ではなく1で考える
	p_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	p_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);


	if (!p_Device)
	{
		return;
	}


	//テクスチャの幅、高さ取得
	float cw = Texture_GetWidth(textureId)*(u1 - u0);
	float ch = Texture_GetHeight(textureId)*(v1 - v0);

	Vertex2d SquareV[] =
	{
		{D3DXVECTOR4(dx - 0.5f,    dy - 0.5f,    0.0f,1.0f),g_color,u0,v0},
		{D3DXVECTOR4(dx + cw - 0.5f,dy - 0.5f,    0.0f,1.0f),g_color,u1,v0},
		{D3DXVECTOR4(dx - 0.5f,    dy + ch - 0.5f,0.0f,1.0f),g_color,u0,v1},
		{D3DXVECTOR4(dx + cw - 0.5f,dy + ch - 0.5f,0.0f,1.0f),g_color,u1,v1}
	};

	p_Device->SetFVF(FVF_VERTEX2D);	//FVFのセット
	p_Device->SetTexture(0, Texture_GetTexture(textureId));		//画像セット
	p_Device->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, &SquareV, sizeof(Vertex2d));
}
void Sprite_Draw(int textureId, float dx, float dy, float uv_x, float uv_y, float uv_w, float uv_h, int radian)
{
	LPDIRECT3DDEVICE9 My_SpriteDevice = MYDirect3D_GetDevice();		//デバイス情報を受け取る変数
	if (!My_SpriteDevice)
	{
		MessageBox(NULL, "デバイスの読み込みに失敗しました。もう一度コードを見直して下さい", "Base", MB_OK | MB_ICONSTOP);
	}

	int w = Texture_GetWidth(textureId);	//テクスチャの幅情報もらう
	int h = Texture_GetHeight(textureId);	//テクスチャの高さ情報もらう

	Vertex2d SquareV[]
	{
		D3DXVECTOR4((dx - w * 0.5f - 0.5f - dx)* cosf(D3DXToRadian(radian)) - (dy - h * 0.5f - 0.5f - dy)* sinf(D3DXToRadian(radian)) + dx,
		(dx - w * 0.5f - 0.5f - dx) * sinf(D3DXToRadian(radian)) + (dy - h * 0.5f - 0.5f - dy)*cosf(D3DXToRadian(radian)) + dy,
		0.0f,1.0f),g_color,uv_x,uv_y,

		D3DXVECTOR4((dx + w * 0.5f - 0.5f - dx)* cosf(D3DXToRadian(radian)) - (dy - h * 0.5f - 0.5f - dy)* sinf(D3DXToRadian(radian)) + dx,
		(dx + w * 0.5f - 0.5f - dx) * sinf(D3DXToRadian(radian)) + (dy - h * 0.5f - 0.5f - dy)*cosf(D3DXToRadian(radian)) + dy,
		0.0f,1.0f),g_color,uv_x + uv_w,uv_y,

		D3DXVECTOR4((dx - w * 0.5f - 0.5f - dx)* cosf(D3DXToRadian(radian)) - (dy + h * 0.5f - 0.5f - dy)* sinf(D3DXToRadian(radian)) + dx,
		(dx - w * 0.5f - 0.5f - dx) * sinf(D3DXToRadian(radian)) + (dy + h * 0.5f - 0.5f - dy)*cosf(D3DXToRadian(radian)) + dy,
		0.0f,1.0f),g_color,uv_x,uv_y + uv_h,

		D3DXVECTOR4((dx + w * 0.5f - 0.5f - dx)* cosf(D3DXToRadian(radian)) - (dy + h * 0.5f - 0.5f - dy)* sinf(D3DXToRadian(radian)) + dx,
		(dx + w * 0.5f - 0.5f - dx) * sinf(D3DXToRadian(radian)) + (dy + h * 0.5f - 0.5f - dy)*cosf(D3DXToRadian(radian)) + dy,
		0.0f,1.0f),g_color,uv_x + uv_w,uv_y + uv_h
	};

	My_SpriteDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	My_SpriteDevice->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, 16);
	My_SpriteDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);// 引数の成分を乗算する
	My_SpriteDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	My_SpriteDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
	My_SpriteDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	My_SpriteDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	My_SpriteDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	My_SpriteDevice->SetFVF(FVF_VERTEX2D);	//FVFのセット
	My_SpriteDevice->SetTexture(0, Texture_GetTexture(textureId));		//画像セット
	My_SpriteDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, SquareV, sizeof(Vertex2d));	//表示
}

/************************************************************
*				テクスチャの切り取り(ピクセル数)			*
*************************************************************/
void Sprite_Draw_Cut(int textureId, float dx, float dy, int cut_x, int cut_y, int cut_w, int cut_h)
{
	LPDIRECT3DDEVICE9 My_SpriteDevice = MYDirect3D_GetDevice();		//デバイス情報を受け取る変数

	int w = Texture_GetWidth(textureId);	//テクスチャの幅情報もらう
	int h = Texture_GetHeight(textureId);	//テクスチャの高さ情報もらう

	float u0 = cut_x / (float)w;
	float v0 = cut_y / (float)h;
	float u4 = (cut_x + cut_w) / (float)w;
	float v4 = (cut_y + cut_h) / (float)h;

	Vertex2d SquareV[]
	{
		D3DXVECTOR4(dx - cut_w * 0.5f - 0.5f,dy - cut_h * 0.5f - 0.5f,0.0f,1.0f),g_color,u0,v0,

		D3DXVECTOR4(dx + cut_w * 0.5f - 0.5f,dy - cut_h * 0.5f - 0.5f,0.0f,1.0f),g_color,u4,v0,

		D3DXVECTOR4(dx - cut_w * 0.5f - 0.5f,dy + cut_h * 0.5f - 0.5f,0.0f,1.0f),g_color,u0,v4,

		D3DXVECTOR4(dx + cut_w * 0.5f - 0.5f,dy + cut_h * 0.5f - 0.5f,0.0f,1.0f),g_color,u4,v4
	};

	My_SpriteDevice->SetTexture(0, Texture_GetTexture(textureId));		//画像セット
	My_SpriteDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, SquareV, sizeof(Vertex2d));	//普通の表示
}

/************************************************************
*						回転(中心変更)						*
*************************************************************/
void Sprite_Draw_Rotation(int textureId, float dx, float dy, int cut_x, int cut_y, int cut_w, int cut_h, float center_x, float center_y, float angle)
{
	LPDIRECT3DDEVICE9 My_SpriteDevice = MYDirect3D_GetDevice();		//デバイス情報を受け取る変数
	if (!My_SpriteDevice)
	{
		MessageBox(NULL, "デバイスの読み込みに失敗しました。もう一度コードを見直して下さい", "Base", MB_OK | MB_ICONSTOP);
	}

	//クラス(floatが16ある、4×4行列)
	D3DXMATRIX mtxR;					//普通の変数と同じで宣言したてはゴミが入っている	
	D3DXMATRIX mtxT;					//普通の変数と同じで宣言したてはゴミが入っている
	D3DXMATRIX mtxIT;					//普通の変数と同じで宣言したてはゴミが入っている

	D3DXMatrixRotationZ(&mtxR, D3DXToRadian(angle));		//Z軸回転を行う(出力行列,回転角(ラジアン角))
	D3DXMatrixTranslation(&mtxT, -center_x, -center_y, 0.0f);	//移動(原点に(0,0,0))(出力行列,<X,Y,Z>)
	D3DXMatrixTranslation(&mtxIT, center_x, center_y, 0.0f);	//移動(元の位置に戻す)(出力行列,<X,Y,Z>)

	D3DXMATRIX mtxW = mtxT * mtxR*mtxIT;	//行列の合成(合成する順番には気をつけること)

	int w = Texture_GetWidth(textureId);	//テクスチャの幅情報もらう
	int h = Texture_GetHeight(textureId);	//テクスチャの高さ情報もらう

	float u0 = cut_x / (float)w;
	float v0 = cut_y / (float)h;
	float u4 = (cut_x + cut_w) / (float)w;
	float v4 = (cut_y + cut_h) / (float)h;

	Vertex2d SquareV[]
	{
		D3DXVECTOR4(dx - cut_w * 0.5f - 0.5f,dy - cut_h * 0.5f - 0.5f,0.0f,1.0f),g_color,u0,v0,

		D3DXVECTOR4(dx + cut_w * 0.5f - 0.5f,dy - cut_h * 0.5f - 0.5f,0.0f,1.0f),g_color,u4,v0,

		D3DXVECTOR4(dx - cut_w * 0.5f - 0.5f,dy + cut_h * 0.5f - 0.5f,0.0f,1.0f),g_color,u0,v4,

		D3DXVECTOR4(dx + cut_w * 0.5f - 0.5f,dy + cut_h * 0.5f - 0.5f,0.0f,1.0f),g_color,u4,v4
	};

	for (int i = 0; i < 4; i++)
	{
		D3DXVec4Transform(&SquareV[i].position, &SquareV[i].position, &mtxW);	//座標変換
	}

	My_SpriteDevice->SetFVF(FVF_VERTEX2D);	//FVFのセット
	My_SpriteDevice->SetTexture(0, Texture_GetTexture(textureId));		//画像セット
	My_SpriteDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, SquareV, sizeof(Vertex2d));	//表示
}

void Sprite_Draw_Rotate(int textureId, float dx, float dy, int cut_x, int cut_y, int cut_w, int cut_h, float center_x, float center_y, float angle)
{
		LPDIRECT3DDEVICE9 My_SpriteDevice = MYDirect3D_GetDevice();		//デバイス情報を受け取る変数
	if (!My_SpriteDevice)
	{
		MessageBox(NULL, "デバイスの読み込みに失敗しました。もう一度コードを見直して下さい", "Base", MB_OK | MB_ICONSTOP);
	}

	//クラス(floatが16ある、4×4行列)
	D3DXMATRIX mtxR;					//普通の変数と同じで宣言したてはゴミが入っている	
	D3DXMATRIX mtxT;					//普通の変数と同じで宣言したてはゴミが入っている
	D3DXMATRIX mtxIT;					//普通の変数と同じで宣言したてはゴミが入っている

	D3DXMatrixRotationZ(&mtxR, D3DXToRadian(angle));		//Z軸回転を行う(出力行列,回転角(ラジアン角))
	D3DXMatrixTranslation(&mtxT, -center_x, -center_y, 0.0f);	//移動(原点に(0,0,0))(出力行列,<X,Y,Z>)
	D3DXMatrixTranslation(&mtxIT, center_x, center_y, 0.0f);	//移動(元の位置に戻す)(出力行列,<X,Y,Z>)

	D3DXMATRIX mtxW = mtxT * mtxR*mtxIT;	//行列の合成(合成する順番には気をつけること)

	int w = Texture_GetWidth(textureId);	//テクスチャの幅情報もらう
	int h = Texture_GetHeight(textureId);	//テクスチャの高さ情報もらう

	float u0 = cut_x / (float)w;
	float v0 = cut_y / (float)h;
	float u4 = (cut_x + cut_w) / (float)w;
	float v4 = (cut_y + cut_h) / (float)h;

	Vertex2d SquareV[]
	{
		D3DXVECTOR4(CenterRotateX(dx, dy,dx - cut_w * 0.5f - 0.5f, dy - cut_h * 0.5f - 0.5f, angle),CenterRotateY(dx, dy,dx - cut_w * 0.5f - 0.5f, dy - cut_h * 0.5f - 0.5f, angle),0.0f,1.0f),g_color,u0,v0,
		D3DXVECTOR4(CenterRotateX(dx, dy,dx + cut_w * 0.5f - 0.5f, dy - cut_h * 0.5f - 0.5f, angle),CenterRotateY(dx, dy,dx + cut_w * 0.5f - 0.5f, dy - cut_h * 0.5f - 0.5f, angle),0.0f,1.0f),g_color,u4,v0,
		D3DXVECTOR4(CenterRotateX(dx, dy,dx - cut_w * 0.5f - 0.5f, dy + cut_h * 0.5f - 0.5f, angle),CenterRotateY(dx, dy,dx - cut_w * 0.5f - 0.5f, dy + cut_h * 0.5f - 0.5f, angle),0.0f,1.0f),g_color,u0,v4,
		D3DXVECTOR4(CenterRotateX(dx, dy,dx + cut_w * 0.5f - 0.5f, dy + cut_h * 0.5f - 0.5f, angle),CenterRotateY(dx, dy,dx + cut_w * 0.5f - 0.5f, dy + cut_h * 0.5f - 0.5f, angle),0.0f,1.0f),g_color,u4,v4,
	};

	//for (int i = 0; i < 4; i++)
	//{
	//	D3DXVec4Transform(&SquareV[i].position, &SquareV[i].position, &mtxW);	//座標変換
	//}

	My_SpriteDevice->SetFVF(FVF_VERTEX2D);	//FVFのセット
	My_SpriteDevice->SetTexture(0, Texture_GetTexture(textureId));		//画像セット
	My_SpriteDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, SquareV, sizeof(Vertex2d));	//表示
}

/************************************************************
*							拡大							*
*************************************************************/
void Sprite_Drow_Expansion(int textureId, float dx, float dy, float uv_x, float uv_y, float uv_w, float uv_h, float Xscale, float Yscale, float Zscale)
{
	LPDIRECT3DDEVICE9 My_SpriteDevice = MYDirect3D_GetDevice();		//デバイス情報を受け取る変数
	if (!My_SpriteDevice)
	{
		MessageBox(NULL, "デバイスの読み込みに失敗しました。もう一度コードを見直して下さい", "Base", MB_OK | MB_ICONSTOP);
	}

	D3DXMATRIX mtxE;					//普通の変数と同じで宣言したてはゴミが入っている
	D3DXMATRIX mtxT;					//普通の変数と同じで宣言したてはゴミが入っている
	D3DXMATRIX mtxIT;					//普通の変数と同じで宣言したてはゴミが入っている
	//行列へのポインタ,Xスケール,Yスケール,Zスケール(1.0f)が普通
	D3DXMatrixScaling(&mtxE, Xscale, Yscale, Zscale);
	D3DXMatrixTranslation(&mtxT, -dx, -dy, 0.0f);	//移動(原点に(0,0,0))(出力行列,<X,Y,Z>)
	D3DXMatrixTranslation(&mtxIT, dx, dy, 0.0f);	//移動(元の位置に戻す)(出力行列,<X,Y,Z>)

	D3DXMATRIX mtxW = mtxT * mtxE*mtxIT;	//行列の合成(合成する順番には気をつけること)

	int w = Texture_GetWidth(textureId);	//テクスチャの幅情報もらう
	int h = Texture_GetHeight(textureId);	//テクスチャの高さ情報もらう

	Vertex2d SquareV[]
	{
		D3DXVECTOR4(dx - w * 0.5f - 0.5f,dy - h * 0.5f - 0.5f ,0.0f,1.0f),g_color,uv_x,uv_y,

		D3DXVECTOR4(dx + w * 0.5f - 0.5f,dy - h * 0.5f - 0.5f,0.0f,1.0f),g_color,uv_x + uv_w,uv_y,

		D3DXVECTOR4(dx - w * 0.5f - 0.5f,dy + h * 0.5f - 0.5f,0.0f,1.0f),g_color,uv_x,uv_y + uv_h,

		D3DXVECTOR4(dx + w * 0.5f - 0.5f,dy + h * 0.5f - 0.5f,0.0f,1.0f),g_color,uv_x + uv_w,uv_y + uv_h
	};

	for (int i = 0; i < 4; i++)
	{
		D3DXVec4Transform(&SquareV[i].position, &SquareV[i].position, &mtxW);	//座標変換
	}

	My_SpriteDevice->SetFVF(FVF_VERTEX2D);	//FVFのセット
	My_SpriteDevice->SetTexture(0, Texture_GetTexture(textureId));		//画像セット
	My_SpriteDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, SquareV, sizeof(Vertex2d));	//表示
}

//かんくん
float RotateX(float x, float y, float rotate)
{
	return x * cos(rotate) - y * sin(rotate);
}
float RotateY(float x, float y, float rotate)
{
	return x * sin(rotate) + y * cos(rotate);
}

float CenterRotateX(float centerx, float centery, float x, float y, float rotate)
{
	return centerx + RotateX(centerx - x, centery - y, rotate - D3DX_PI / 2);
}

float CenterRotateY(float centerx, float centery, float x, float y, float rotate)
{
	return centery + RotateY(centerx - x, centery - y, rotate - D3DX_PI / 2);
}

float cameraX(float objectx)
{
	return SCREHN_WIDTH / 2 + objectx - c_Player->position.x;
}

float cameraY(float objecty)
{
	return SCREHN_HEIGHT / 2 + objecty - c_Player->position.y;
}
