#pragma once
#include "common.h"
#include "main.h"
#include "Direct3D.h"

#define MAXBLOCK (300)

enum editorType
{
	EDITNONE,
	BLOCK,
	ENEMY,
	EDITMAX,
};

struct ASTEROID
{
	float posx;
	float posy;
	float angle;
	static int TextureIDAsteroid;
};

struct DIRTWALL
{
	float posx;
	float posy;
	float angle;
	static int TextureIDBlock;
};

struct ENEMYEDIT
{
	float posx;
	float posy;
	float angle;
};


struct MAPEDITOR
{
	ASTEROID asteroid;
	DIRTWALL dirtwall;
	ENEMYEDIT enemyedit;

	float posx;
	float posy;
	float angle;
	bool  place;
	bool  erase;
	
	int   maxBlock;
	int   maxEnemy;
};

void InitMapEditor();

void UpdateMapEditor();

void UninitMapEditor();

void DrawMapEditor();