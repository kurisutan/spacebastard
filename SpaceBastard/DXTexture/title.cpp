#define _CRT_SECURE_NO_WARNINGS
#include "stdio.h"
#include "bullet.h"
#include "player.h"
#include "texture.h"
#include "sprite.h"
#include "collision.h"
#include "background.h"
#include "title.h"
#include "common.h"
#include "string.h"
#include "game.h"
#include "input.h"
#include "fade.h"

static float Texture_IDTitle = 0;
static float Texture_IDPlay = 0;
static float Texture_IDTitleBackground = 0;
int timer;
int digit1 = 0;
int digit2 = 0;
int digit3 = 0;

char bganimation[20];
char bganim[20];


void InitTitle()
{
	Texture_IDTitle = Texture_SetLoadFile("rom/Title/title.png", 659 * 2, 59 * 2);				//読み込みの準備
	Texture_IDPlay = Texture_SetLoadFile("rom/Title/pressenter.png", 1172, 50);				//読み込みの準備
	Texture_IDTitleBackground = Texture_SetLoadFile("rom/Title/bganim/bg00001.png", 1980, 1080);				//読み込みの準備
	Texture_Load();
}
void UninitTitle()
{
	Texture_Release();
}
void UpdateTitle()
{
	timer++;
	if (timer > 2)
	{
		digit1++;
		strcpy(bganimation, loadSpecialAwesome("rom/Title/bganim/bg00%d%d%d.png", digit3, digit2, digit1));

		digit1++;

		int num = digit3 * 100 + digit2 * 10 + digit1;
		if (num > 449)
		{
			digit1  = 0;
			digit2  = 0;
			digit3  = 0;
			return;
		}

		Texture_IDTitleBackground = Texture_SetLoadFile(bganimation, 1980, 1080);
		Texture_Load();

		if (digit1 / 10 == 1)
		{
			digit2++;
			digit1 = 0;
		}
		if (digit2 / 10 == 1)
		{
			digit3++;
			digit2 = 0;
			digit1 = 0;
		}
		timer = 0;

		//if (!flip)
		//{

		//	Texture_IDTitleBackground = Texture_SetLoadFile("bganim/bg1.png", 1980, 1080);				//読み込みの準備
		//	Texture_Load();
		//	flip = true;
		//}
		//else if (flip)
		//{
		//	Texture_IDTitleBackground = Texture_SetLoadFile("bganim/bg0.png", 1980, 1080);				//読み込みの準備
		//	Texture_Load();
		//	flip = false;
		//}
	}
	if (Keyboard_IsPress(DIK_RETURN))
	{
		Fade(SCENE_STAGE1);
	}
	//for (int j = 0; j < 30; j++)
	//{
	//	for (int i = 0; i < 20; i++)
	//	{
	//		strcpy(bganimation, bganim);
	//	}
	//	Texture_IDTitleBackground = Texture_SetLoadFile(bganimation, 1980, 1080);				//読み込みの準備
	//}

}
void DrawTitle()
{
	Sprite_Draw(Texture_IDTitleBackground, SCREHN_WIDTH / 2, SCREHN_HEIGHT / 2, 1980, 1080, 1.0f, 1.0f, 0);
	Sprite_Draw(Texture_IDTitle, SCREHN_WIDTH /2, 200, 659 * 2, 59 * 2, 1.0f, 1.0f, 0);
	Sprite_Draw(Texture_IDPlay, SCREHN_WIDTH / 2, 800, 1172, 50, 1.0f, 1.0f, 0);

}

char* loadSpecialAwesome(const char *pFormat, ...)
{
	va_list argp;
	va_start(argp, pFormat);
	char buf[50];
	vsprintf(buf, pFormat, argp);
	va_end(argp);

	return buf;
}

