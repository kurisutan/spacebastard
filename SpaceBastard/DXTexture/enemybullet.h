#pragma once
#include "Direct3D.h"
#include "collision.h"
#include "enemy.h"
#define E_MAXBULLET (100)
/***************************************
			プロトタイプ宣言
****************************************/
enum E_BULLETTYPE
{
	ENORMAL,
	TRIPPLE,
	FREEZE,
};

struct E_SHOT
{
	int x;
	int y;
};


struct E_BULLET
{
	D3DXVECTOR2 position;
	double angle;
	bool isUseBullet;
	int bulletSpeedx;
	int bulletSpeedy;
	int bulletAmount;
	CIRCLE bulletcollision;
	E_BULLETTYPE bulletType;
	E_SHOT maxBullet[E_MAXBULLET];
};
void enemybulletInit();		//初期化
void enemybulletUnnit();		//終了処理
void enemybulletUpdate();		//更新
void enemybulletDrow();		//描画
void CreateEnemyBullet(E_BULLETTYPE type, float x, float y, float speedX, float speedY);
CIRCLE GetEnemyBulletCollision(int index);
E_BULLET* GetEnemyBullet();
