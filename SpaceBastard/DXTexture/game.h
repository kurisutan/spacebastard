#pragma once//インクルードガード
#ifndef _GAME_H_
#define _GAME_H_

enum SCENE
{
	SCENE_NONE,
	SCENE_MAPEDITOR,
	SCENE_TITLE,
	SCENE_STAGE1,
	SCENE_RESULT
};

struct MAINMENU
{
	SCENE scene;
};


/****************************************
				定数定義
*****************************************/


/***************************************
			   インクルード
****************************************/


/***************************************
			プロトタイプ宣言
****************************************/
void gameInit();		//初期化
void gameUnnit();		//終了処理
void gameUpdate();		//更新
void gameDrow();		//描画
void SetScene(SCENE Scene);
MAINMENU *GetScene();

#endif 