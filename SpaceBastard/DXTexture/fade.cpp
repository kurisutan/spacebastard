
#include "main.h"
#include "texture.h"
#include "sprite.h"
#include "fade.h"
#include "Direct3D.h"

#define FVF_FADEVERTEX (D3DFVF_XYZRHW | D3DFVF_DIFFUSE)


typedef struct FadeVertex_tag
{
	D3DXVECTOR4 position;
	D3DCOLOR    color;

}FadeVertex;


FADE_STATE g_FadeState;         //シーン保存
SCENE      g_FadeNextScene;     //Fade関数用
float      g_FadeAlpha;         //フェードポリゴンのα値


void InitFade()
{
	g_FadeState = FADE_STATE_NONE;
	g_FadeNextScene = SCENE_NONE;
	g_FadeAlpha = 0.0f;
}


void UninitFade()
{
	Texture_Release();
}


void UpdateFade()
{
	if (g_FadeState == FADE_STATE_OUT)
	{
		if (g_FadeAlpha >= 1.0f)              //フェードポリゴンが真っ黒になったら
		{
			g_FadeAlpha = 1.0f;
			g_FadeState = FADE_STATE_IN;			SetScene(g_FadeNextScene);
		}
		g_FadeAlpha += 1.0f / 60.0f;          //20フレームでフェードアウト
	}
	else if (g_FadeState == FADE_STATE_IN)
	{
		if (g_FadeAlpha <= 0.0f)              //フェードポリゴンが完全に透明になったら
		{
			g_FadeAlpha = 0.0f;
			g_FadeState = FADE_STATE_NONE;
		}
		g_FadeAlpha -= 1.0f / 60.0f;          //20フレームでフェードアウト
	}
}

void DrawFade()
{
	if (g_FadeState == FADE_STATE_NONE)
		return;

	FadeVertex v[] = {
		{D3DXVECTOR4(0.0f,0.0f,0.0f,1.0f),D3DXCOLOR(0,0,0,g_FadeAlpha),},
		{D3DXVECTOR4(1920,0.0f,0.0f,1.0f),D3DXCOLOR(0,0,0,g_FadeAlpha),},
		{D3DXVECTOR4(0.0f,1080,0.0f,1.0f),D3DXCOLOR(0,0,0,g_FadeAlpha),},
		{D3DXVECTOR4(1920,1080,0.0f,1.0f),D3DXCOLOR(0,0,0,g_FadeAlpha),}
	};

	LPDIRECT3DDEVICE9 pDevice = MYDirect3D_GetDevice();
	pDevice->SetFVF(FVF_FADEVERTEX);
	pDevice->SetTexture(0, NULL);
	pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, &v, sizeof(FadeVertex));	//表示
}


void Fade(SCENE NextScene)
{
	g_FadeNextScene = NextScene;              //シーンの保存
	g_FadeState = FADE_STATE_OUT;
}


FADE_STATE GetFadeState()
{
	return g_FadeState;
}

void DrawFilledSquare(int cx, int cy, int size)
{

	LPDIRECT3DDEVICE9 pDevice = MYDirect3D_GetDevice();

	Vertex2d g_Vertex[]
	{
		//Basic Square
		{D3DXVECTOR4(cx - size ,cy - size,0.0,1.0),D3DCOLOR_XRGB(255, 0, 0),},
		{D3DXVECTOR4(cx + size ,cy - size,0.0,1.0),D3DCOLOR_XRGB(0, 255, 0),},
		{D3DXVECTOR4(cx - size ,cy + size,0.0,1.0),D3DCOLOR_XRGB(0, 0, 255),},
		{D3DXVECTOR4(cx + size ,cy + size,0.0,1.0),D3DCOLOR_XRGB(255, 0, 0),},
	};

	pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, g_Vertex, sizeof(Vertex2d));
}
