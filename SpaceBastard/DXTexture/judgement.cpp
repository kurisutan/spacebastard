#include "judgement.h"
#include "player.h"
#include "bullet.h"
#include "enemy.h"
#include "EnemyMakerr.h"
#include "collision.h"
#include "explosion.h"
#include "items.h"
#include "input.h"
#include "specialweapon.h"
#include "score.h"
#include "enemybullet.h"

static BULLET* pBullet;
static ENEMY* pEnemy;
static PLAYER* pPlayer;
static ITEM* pItem;
static SPECIALWEAPON* pSp;
static E_BULLET* pEnemyBullet;

static int framecount;
static int timer;
void InitJudgement()
{

}

void UpdateJudgement()
{
	pBullet = GetBullet();
	pEnemy = GetEnemy();
	pItem = GetItem();
	pPlayer = GetPlayer();
	pSp = GetSpecialWeapon();
	pEnemyBullet = GetEnemyBullet();

	//For Items
	if (Keyboard_IsTrigger(DIK_J))
	{
		CreateItem(2, 500, 500);
	}

	for (int i = 0; i < MAXBULLET; i++)
	{
		if ((pEnemyBullet + i)->isUseBullet)
		{
			if (&GetEnemyBulletCollision(i), &GetPlayerCollision())
			{

			}
		}
	}

	for (int i = 0; i < MAXITEM; i++)
	{
		if (pItem[i].isUseItem == true)
		{
			if (pItem[i].isUseItem1 == true)
			{
				if (IsHitCollision_Circle_Circle(&GetPlayerCollision(), &GetItemCollision(i)))
				{
					for (int j = 0; j < MAXBULLET; j++)
					{
						(pBullet + j)->bulletType = UPGRADED;
					}
					pItem[i].isUseItem1 = false;
				}
			}
			else if (pItem[i].isUseItem2 == true)
			{
				if (IsHitCollision_Circle_Circle(&GetPlayerCollision(), &GetItemCollision(i)))
				{
					for (int j = 0; j < MAXBULLET; j++)
					{
						pSp->type = SPSAW;
					}
					pItem[i].isUseItem2 = false;
				}
			}
			else pItem[i].isUseItem = false;
		}
	}

	//Problem that needs to be fixed
	if (pSp->isUseSp)
	{
		framecount++;
		if (framecount >= 5)
		{
			timer++;
			framecount = 0;
		}
		for (int j = 0; j < MAXENEMY; j++)
		{
			if (pEnemy[j].isUseEnemy == true)
			{
				if (IsHitCollision_Circle_Circle(&GetSpCollision(), &GetEnemyCollision(j)))
				{
					if (timer >= 1.0f)
					{
						(pEnemy + j)->basicenemyHP -= 1;
						timer = 0;
						break;
					}
				}
			}
		}
	}
	else framecount = 0;

	for (int i = 0; i < MAXBULLET; i++)
	{
		for (int j = 0; j < MAXENEMY; j++)
		{
			if (pEnemy[j].isUseEnemy == true)
			{
				if ((pEnemy + j)->basicenemyHP == 0)
				{
					PlusScore(1000);
					pEnemy->enemydeathCount++;

					if (pEnemy->enemydeathCount == 1)
					{
  						CreateItem(1,(pEnemy + j)->position.x, (pEnemy + j)->position.y);
					}
					else if (pEnemy->enemydeathCount == 20)
					{
						CreateItem(2, (pEnemy + j)->position.x, (pEnemy + j)->position.y);
					}
					CreateExplosion((pEnemy + j)->position.x, (pEnemy + j)->position.y);
					pEnemy[j].isUseEnemy = false;
					break;
				}
				if (pBullet[i].isUseBullet == true)
				{
					if (IsHitCollision_Circle_Circle(&GetEnemyCollision(j), &GetBulletCollision(i)))
					{
						(pEnemy + j)->basicenemyHP -= 1;
						pBullet[i].isUseBullet = false;
						break;
					}
				}
			}
		}
	}
}