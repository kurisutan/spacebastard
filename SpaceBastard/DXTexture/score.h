#pragma once
#ifndef _SCORE_H_
#define _SCORE_H_

struct SCORE
{
	int number;
};

void Score_Init(void);
void Score_Uninit(void);
void Score_Update(void);
void Score_Draw(int score, float x, float y, int digit, bool bZero, bool bLeft);  //bZrero 0���� ,bLeft ���l��
int PlusScore(int score);
SCORE *GetScore();

#endif
