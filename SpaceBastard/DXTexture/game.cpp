
/***************************************************************************************************************************
＜ゲーム処理＞
2019年07月08日（月曜日）　牧尾　亜人
***************************************************************************************************************************/
#include "player.h"
#include "SpriteAnim.h"
#include "bullet.h"
#include "EnemyMakerr.h"
#include "enemy.h"
#include "enemybullet.h"
#include "specialweapon.h"
#include "judgement.h"
#include "explosion.h"
#include "sprite.h"
#include "items.h"
#include "background.h"
#include "game.h"
#include "title.h"
#include "stage_one.h"
#include "results.h"
#include "fade.h"
#include "score.h"
#include "input.h"
#include "mapeditor.h"

SCENE g_scene = SCENE_NONE;
MAINMENU g_menu;
SCORE *pScore;

/************************************************************
*						初期化								*
*************************************************************/
void gameInit()
{
	Score_Init();
	Sprite_Init();
	SpriteAnim_Init();
	playerInit();		//初期化
	enemyInit();
	enemybulletInit();
	bulletInit();
	spWeaponInit();
	explosionInit();
	itemInit();
	backgroundInit();
	if (g_scene == SCENE_NONE)
	{
		InitFade();
		SetScene(SCENE_TITLE);
	}
	else if (g_scene == SCENE_STAGE1)
	{
		InitStageone();
	}
}

/************************************************************
*						終了処理							*
*************************************************************/
void gameUnnit()
{
	Score_Uninit();
	playerUnnit();		//終了処理
	enemyUnnit();
	enemybulletUnnit();
	bulletUnnit();
	explosionUninit();
	spWeaponUninit();
	itemUninit();
	backgroundUninit();
	UninitStageone();
	UninitFade();
}

/************************************************************
*							更新						    *
*************************************************************/
void gameUpdate()
{
	switch (g_scene)
	{
	case SCENE_TITLE:
		UpdateTitle();
		if (Keyboard_IsTrigger(DIK_TAB))
		{
			if (g_scene == SCENE_TITLE)
			{
				SetScene(SCENE_MAPEDITOR);
			}
		}
		break;
	case SCENE_MAPEDITOR:
		UpdateMapEditor();	
		if (Keyboard_IsTrigger(DIK_TAB))
		{
			if (g_scene == SCENE_MAPEDITOR)
			{
				SetScene(SCENE_STAGE1);
			}
		}
		break;
	case SCENE_STAGE1:
		playerUpdate();	//更新
		enemyUpdate();
		enemybulletUpdate();
		bulletUpdate();
		spWeaponUpdate();
		explosionUpdate();
		itemUpdate();
		backgroundUpdate();
		UpdateStageone();
		UpdateJudgement();
		break;
	case SCENE_RESULT:
		UpdateResults();
		break;
	}
	g_menu.scene = g_scene;
}

/************************************************************
*							描画							*
*************************************************************/
void gameDrow()
{
	pScore = GetScore();
	switch (g_scene)
	{
	case SCENE_TITLE:
		DrawTitle();
		break;
	case SCENE_MAPEDITOR:
		DrawMapEditor();
		backgroundDraw();
	case SCENE_STAGE1:
		backgroundDraw();
		DrawMapEditor();
		spWeaponDraw();
		playerDrow();		//描画
		EnemyDrow();
		enemybulletDrow();
		bulletDrow();
		explosionDrow();
		itemDrow();
		DrawStageone();
		Score_Draw(pScore->number, 100.0f, 100.0f, 7, false, true);
		break;
	case SCENE_RESULT:
		DrawResults();
		break;
	}

	DrawFade();

}

void SetScene(SCENE Scene)
{
	//前シーン終了
	switch (g_scene)
	{
	case SCENE_NONE:
		break;
	case SCENE_TITLE:
		UninitTitle();
		break;
	case SCENE_MAPEDITOR:
		break;
	case SCENE_STAGE1:
		gameUnnit();
		break;
	case SCENE_RESULT:
		UninitResults();
		break;
	}
	//シーンの更新・初期化
	g_scene = Scene;
	switch (g_scene)
	{
	case SCENE_TITLE:
		InitTitle();
		break;
	case SCENE_MAPEDITOR:
		InitMapEditor();
		backgroundInit();
		playerInit();
		break;
	case SCENE_STAGE1:
		gameInit();
		break;
	case SCENE_RESULT:
		InitResults();
		break;
	}
}

MAINMENU *GetScene()
{
	return &g_menu;
}
