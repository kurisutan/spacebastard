#include	"texture.h"
#include	"Direct3D.h"
#include	"sprite.h"
#include	"SpriteAnim.h"
/**************************************
			　グローバル変数
**************************************/
static LPDIRECT3DTEXTURE9  pTexture = NULL;		//画像データ格納

static int g_FrameCounter = 0;					//フレームカウンター
static int Texture_ID_A = 0;
static int Texture_ID_B = 0;
static int pattern;
/************************************************************
*						初期化								*
*************************************************************/
void SpriteAnim_Init(void)
{
	g_FrameCounter = 0;
	pattern = 0;
	pTexture = Texture_GetTexture(Texture_ID_A);		//テクスチャデバイスの受け取り
	Texture_Load();		//読み取り
}

/************************************************************
*						終了処理							*
*************************************************************/
void SpriteAnim_Uninit(void)
{
	SAFE_RELEASE(pTexture);		//解放
	Texture_Release();			//テクスチャデータの解放
}

/************************************************************
*							更新						    *
*************************************************************/
void SpriteAnim_Update(void)
{
	g_FrameCounter++;
}

/************************************************************
*							描画							*
*************************************************************/
void SpriteAnim_Drow(float dx, float dy)
{
	int patten = g_FrameCounter / 10 % 5;
	Sprite_Draw_Cut(Texture_ID_A, dx, dy, 16*patten, 0, 16, 16);
}

void SpriteAnim_Drow(int TextureID, float dx, float dy, float cut_dx, float cut_dy, float cut_x,float cut_y)
{
	pattern = (int)g_FrameCounter / 5;


	cut_x = pattern % 5 * cut_x;
	cut_y = pattern / 5 * cut_y;

	Sprite_Draw_Rotation(TextureID, dx, dy, cut_x, cut_y, cut_dx, cut_dy, dx, dy, 90);
}
void SpriteAnim_Drow(int TextureID, float dx, float dy, float cut_dx, float cut_dy, float cut_x, float cut_y, float angle)
{
	pattern = (int)g_FrameCounter / 5;

	cut_x = pattern % 5 * cut_x;
	cut_y = pattern / 5 * cut_y;

	Sprite_Draw_Rotate(TextureID, dx, dy, cut_x, cut_y, cut_dx, cut_dy, dx, dy, angle);
}
void SpriteAnim_Drowsp(int TextureID, float dx, float dy, float cut_dx, float cut_dy, float cut_x, float cut_y,int angle)
{
	pattern = (int)g_FrameCounter / 5;


	cut_x = pattern % 5 * cut_x;

	Sprite_Draw_Rotation(TextureID, dx, dy, cut_x, cut_y, cut_dx, cut_dy, dx, dy, angle);
}

void SpriteAnim_DrowFlip(int TextureID, float dx, float dy, float cut_dx, float cut_dy, float cut_x, float cut_y)
{
	pattern = (int)g_FrameCounter / 5;


	cut_x = pattern % 5 * cut_x;
	cut_y = pattern / 5 * cut_y;
	Sprite_Draw_Rotation(TextureID, dx, dy, cut_x, cut_y, cut_dx, cut_dy, dx, dy, 90);

}

void SpriteAnim_DrowExplosion(int ID, float dx, float dy, float cut_x, float cut_y)
{
	pattern = (int)g_FrameCounter / 10;


	cut_x = pattern % 5 * 32;

	Sprite_Draw_Cut(ID, dx, dy, cut_x, cut_y, 32, 32);
}
//cut_x=pattern%5*140; cut_y=pattern/5*200;