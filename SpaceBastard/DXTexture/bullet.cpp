//プレイヤーのバレット

#include "bullet.h"
#include "player.h"
#include "texture.h"
#include "sprite.h"
#include "collision.h"

//Bullet
BULLET g_bullet[MAXBULLET];
//BulletCollision
CIRCLE g_Collision[MAXBULLET];
//P;ayerinfo
PLAYER *b_Player = GetPlayer();



int Texture_IDBullet = 0;	//テクスチャID

void bulletInit()
{
	for (int i = 0; i < MAXBULLET; i++)
	{
		g_bullet[i].isUseBullet = false;
		g_bullet[i].position.x = 0;
		g_bullet[i].position.y = 0;
		g_bullet[i].bulletcollision.centerposition = g_bullet->position;
		g_bullet[i].bulletcollision.radius = 32;
		g_bullet[i].bulletType = NORMAL;
		g_bullet[i].angle = b_Player->facing;
		g_bullet[i].bulletSpeedx = 0;
		g_bullet[i].bulletSpeedx = 0;
	}


	Texture_IDBullet = Texture_SetLoadFile("rom/Player/laser-bolts.png", 64, 64);				//読み込みの準備
	Texture_Load();
}
void bulletUnnit()
{
	Texture_Release();					//テクスチャデータ開放
}
void bulletUpdate()
{
	//Checking all bullets
	for (int i = 0; i < MAXBULLET; i++)
	{
		//If bullet is being used
		if (g_bullet[i].isUseBullet)
		{
			//Updating bullet centercollision with its position
			g_bullet[i].bulletcollision.centerposition = g_bullet[i].position;

			//Checking Bullet type
			if (g_bullet[i].bulletType == NORMAL)
			{
				//Giving bullet its proper angle plus speed
						//かんくん
				g_bullet[i].position.x += g_bullet[i].bulletSpeedx * cosf(g_bullet[i].angle);
				g_bullet[i].position.y += g_bullet[i].bulletSpeedx * sinf(g_bullet[i].angle);

				//Bullets will dissappear according to players coordinates
				if (g_bullet[i].position.x > b_Player->position.x + SCREHN_WIDTH || g_bullet[i].position.x < b_Player->position.x - SCREHN_WIDTH)
				{
					//becomes false
					g_bullet[i].isUseBullet = false;
				}
				//Same for Y
				else if (g_bullet[i].position.y > b_Player->position.y + SCREHN_HEIGHT || g_bullet[i].position.y < b_Player->position.y - SCREHN_HEIGHT)
				{
					g_bullet[i].isUseBullet = false;
				}
			}
			//Upgraded bullets same thing
			else if (g_bullet[i].bulletType == UPGRADED)
			{
				//Giving bullet its proper angle plus speed
				g_bullet[i].position.x += g_bullet[i].bulletSpeedx * cos(g_bullet[i].angle + D3DX_PI / 2);
				g_bullet[i].position.y += g_bullet[i].bulletSpeedy * sin(g_bullet[i].angle + D3DX_PI / 2);

				if (g_bullet[i].position.x > b_Player->position.x + SCREHN_WIDTH || g_bullet[i].position.x < b_Player->position.x - SCREHN_WIDTH || g_bullet[i].position.y > b_Player->position.y + SCREHN_HEIGHT || g_bullet[i].position.y < b_Player->position.y - SCREHN_HEIGHT)
				{
					g_bullet[i].isUseBullet = false;
				}
			}
		}
	}
}

void bulletDrow()
{
	for (int i = 0; i < MAXBULLET; i++)
	{
		if (g_bullet[i].isUseBullet && g_bullet[i].bulletType == NORMAL)
		{
			Sprite_Draw_Rotate(Texture_IDBullet, cameraX(g_bullet[i].position.x), cameraY(g_bullet[i].position.y), 32, 32, 32, 32, g_bullet[i].position.x, g_bullet[i].position.y, g_bullet[i].angle);
		}
		else if (g_bullet[i].isUseBullet && g_bullet[i].bulletType == UPGRADED)
		{
			Sprite_Draw_Rotation(Texture_IDBullet, g_bullet[i].position.x, g_bullet[i].position.y, 32, 32, 32, 32, g_bullet[i].position.x, g_bullet[i].position.y, g_bullet[i].angle);
		}
	}
}

void CreateBullet(float x, float y, float speedX, float speedY,double angle)
{
	for (int i = 0; i < MAXBULLET; i++)
	{
		//If bullets arent being used
		if (!g_bullet[i].isUseBullet)
		{
			if (g_bullet[i].bulletType == NORMAL)
			{
				g_bullet[i].position.x = x;
				g_bullet[i].position.y = y;
				g_bullet[i].bulletcollision.centerposition = g_bullet[i].position;
				g_bullet[i].angle = angle;
				g_bullet[i].bulletSpeedx = speedX;
				g_bullet[i].bulletSpeedy = speedY;
				g_bullet[i].isUseBullet = true;
				break;
			}
			else if (g_bullet[i].bulletType == UPGRADED)
			{
				g_bullet[i].position.x = x;
				g_bullet[i].position.y = y;
				g_bullet[i].bulletcollision.centerposition = g_bullet[i].position;
				g_bullet[i].angle = angle;
				g_bullet[i].bulletSpeedx = speedX;
				g_bullet[i].bulletSpeedy = speedY;
				g_bullet[i].isUseBullet = true;
				break;
			}
		}
	}
}

CIRCLE GetBulletCollision(int index)
{
	return g_bullet[index].bulletcollision;
}

BULLET* GetBullet() 
{
	return g_bullet;
}