#include "Direct3D.h"	
#include "dinputex.h"

/**************************************
			　グローバル変数
**************************************/
static LPDIRECT3D9 g_pD3D = NULL;
static LPDIRECT3DDEVICE9 My_Device = NULL;	//Direct3Dデバイスへのポインター

/************************************************************
*						初期化								*
*************************************************************/
bool MYDirect3D_Init(HWND hwnd)
{
	g_pD3D = Direct3DCreate9(D3D_SDK_VERSION);			//ヴァージョンを指定する
	if (g_pD3D == NULL)									//インターフェイスを取得できない
	{
		MessageBox(hwnd, "Direct3Dの初期化に失敗しました、DirectX 9.0がインストールされているか確認してください。", "Base", MB_OK | MB_ICONSTOP);
		return false;
	}

	//**************************注文書作成***********************
	D3DPRESENT_PARAMETERS d3dpp = {};

	//<BackBuffer>は裏画面 サイズは表面と同じ
	d3dpp.BackBufferWidth = SCREHN_WIDTH;		//裏画面の幅
	d3dpp.BackBufferHeight = SCREHN_HEIGHT;		//裏画面の高さ
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;	//デスクトップに依存する(フルサイズ以外)
	d3dpp.BackBufferCount = 1;					//バックバッファの数

	//<Swap>は裏画面と表画面を入れ替える行為
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;	//スワップの仕方の指定
	d3dpp.Windowed = TRUE;						//ウィンドウモードかフルスクリーンモードかの指定

	//デプスバッファ(深度情報)、ステンシルバッファ
	d3dpp.EnableAutoDepthStencil = TRUE;		//デプスバッファ、ステンシルバッファの使用するかの指定
	//<D3DFMT_D16>は深度情報を16ビットもつ
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;	//デプスバッファを使用する場合は設定する

	d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;		//フルスクリーンの更新速度を設定する(フルスクリーンの時だけ)
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;		//更新するタイミングの指定　ONE(高性能だが処理が重い),IMMEDIATE(ベンチマーク)
	//**************************注文書作成***********************

	//(モニター指定,デバイスタイプ,対象のウィンドウハンドル,頂点計算,注文書のアドレス,デバイスへのポインター)
	HRESULT hr = g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hwnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &My_Device);

	if (FAILED(hr))			//デバイスが完成しなかったら
	{
		MessageBox(hwnd, "デバイスの作成に失敗しました、画面モードが16ビットあるいは32ビットになっていることを確認してください。", "Base", MB_OK | MB_ICONSTOP);
		return false;
	}
	else					//デバイスが完成したら
	{
		return true;
	}


	//FAILED(hr);			//hrが失敗だったら真
	//SUCCEEDED(hr);		//hrが成功だったら真
}


/************************************************************
*						終了処理							*
*************************************************************/
void MYDirect3D_Uninit(void)
{
	SAFE_RELEASE(My_Device);	//解放
	SAFE_RELEASE(g_pD3D);		//解放
}

/************************************************************
*					デバイスの情報をもらう					*
*************************************************************/
LPDIRECT3DDEVICE9 MYDirect3D_GetDevice(void)
{
	return My_Device;
}