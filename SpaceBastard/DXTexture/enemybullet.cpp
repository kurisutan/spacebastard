#include "enemybullet.h"
#include "player.h"
#include "texture.h"
#include "sprite.h"
#include "collision.h"
#include "math.h"
#include "enemy.h"
#define BULLETSPEED (20)

E_BULLET g_enemybullet[E_MAXBULLET];
PLAYER *e_player;
ENEMY *e_enemy;


double x;
static int Texture_IDEnemyBullet = 0;				//テクスチャID
static int Texture_IDEnemySaw = 0;
void enemybulletInit()
{
	e_player = GetPlayer();
	e_enemy = GetEnemy();
	for (int i = 0; i < E_MAXBULLET; i++)
	{
		g_enemybullet[i].isUseBullet = false;
		g_enemybullet[i].position.x = 0;
		g_enemybullet[i].position.y = 0;
		g_enemybullet[i].bulletcollision.centerposition = g_enemybullet->position;
		g_enemybullet[i].bulletcollision.radius = 32;
		g_enemybullet[i].bulletType = ENORMAL;
		g_enemybullet[i].angle = atan2(e_player->position.y - (e_enemy+i)->position.y, e_player->position.x - (e_enemy + i)->position.x);
		g_enemybullet[i].bulletSpeedx = 0;
		g_enemybullet[i].bulletSpeedx = 0;
	}


	Texture_IDEnemyBullet = Texture_SetLoadFile("laser-bolts2.png", 64 * 2, 64 * 2);				//読み込みの準備
	Texture_IDEnemySaw = Texture_SetLoadFile("saw.png", 256 * 2, 32 * 2);				//読み込みの準備
	Texture_Load();
}
void enemybulletUnnit()
{
	Texture_Release();					//テクスチャデータ開放
}
void enemybulletUpdate()
{
	e_enemy = GetEnemy();
	for (int i = 0; i < E_MAXBULLET; i++)
	{
		if (g_enemybullet[i].isUseBullet)
		{
			g_enemybullet[i].bulletcollision.centerposition = g_enemybullet[i].position;
			//Bullet going Y or X
			if (g_enemybullet[i].bulletType == ENORMAL)
			{
				g_enemybullet[i].position.x += 5 * cos(g_enemybullet[i].angle);
				g_enemybullet[i].position.y += 5 * sin(g_enemybullet[i].angle);

				if (g_enemybullet[i].position.x > 2000 || g_enemybullet[i].position.x < -20)
				{
					g_enemybullet[i].isUseBullet = false;
				}
				else if (g_enemybullet[i].position.y > 1100 || g_enemybullet[i].position.y < -20)
				{
					g_enemybullet[i].isUseBullet = false;
				}
			}
			if (g_enemybullet[i].bulletType == TRIPPLE)
			{
				g_enemybullet[i].position.x += g_enemybullet[i].bulletSpeedx;
				g_enemybullet[i].position.y += g_enemybullet[i].bulletSpeedy;

				if (g_enemybullet[i].position.x > 2000 || g_enemybullet[i].position.x < -20 || g_enemybullet[i].position.y > 1080 || g_enemybullet[i].position.y < 0)
				{
					g_enemybullet[i].isUseBullet = false;
				}
				else if (g_enemybullet[i].position.y > 1100 || g_enemybullet[i].position.y < -20)
				{
					g_enemybullet[i].isUseBullet = false;
				}
			}
		}
	}
}

void enemybulletDrow()
{
	e_enemy = GetEnemy();
	for (int i = 0; i < E_MAXBULLET; i++)
	{
		//IF bullet Type is NORMAL
		if (g_enemybullet[i].bulletType == ENORMAL)
		{
			Sprite_Draw_Rotation(Texture_IDEnemyBullet, g_enemybullet[i].position.x - cameraX(e_player->position.x), g_enemybullet[i].position.y - cameraY(e_player->position.y), 64 * 2, 64 * 2, 32 * 2, 32 * 2, g_enemybullet[i].position.x, g_enemybullet[i].position.y, 90);
		}
		if (g_enemybullet[i].bulletType == TRIPPLE)
		{
			Sprite_Draw_Rotation(Texture_IDEnemyBullet, g_enemybullet[i].position.x - cameraX(e_player->position.x), g_enemybullet[i].position.y - cameraY(e_player->position.y), 64 * 2, 64 * 2, 32 * 2, 32 * 2, g_enemybullet[i].position.x, g_enemybullet[i].position.y, 90);
		}
	}
}

void CreateEnemyBullet(E_BULLETTYPE type, float x, float y, float speedX, float speedY)
{
		for (int i = 0; i < E_MAXBULLET; i++)
		{
			if (!g_enemybullet[i].isUseBullet)
			{
				g_enemybullet[i].bulletcollision.centerposition = g_enemybullet[i].position;
				g_enemybullet[i].position.x = x;
				g_enemybullet[i].position.y = y;
				g_enemybullet[i].bulletSpeedx = speedX;
				g_enemybullet[i].bulletSpeedy = speedY;
				if (type == ENORMAL)
				{
					g_enemybullet[i].angle = atan2(e_player->position.y - (e_enemy + i)->position.y, e_player->position.x - (e_enemy + i)->position.x);
					g_enemybullet[i].bulletType = ENORMAL;
				}
				else if (type == TRIPPLE)
				{
					g_enemybullet[i].bulletType = TRIPPLE;
				}
				g_enemybullet[i].isUseBullet = true;
				break;
			
			}
		}

	//for (int j = 0; j < MAXENEMY; j++)
	//{
	//	if (e_enemy[j]->isUseEnemy)
	//	{
	//		for (int i = 0; i < E_MAXBULLET; i++)
	//		{
	//			if (!g_enemybullet[i].isUseBullet)
	//			{
	//				if (e_enemy[j]->enemyType == SMALL)
	//				{
	//					g_enemybullet[i].bulletcollision.centerposition = g_enemybullet[j].position;
	//					g_enemybullet[i].position.x = x;
	//					g_enemybullet[i].position.y = y;
	//					g_enemybullet[i].bulletSpeedx = speedX;
	//					g_enemybullet[i].bulletSpeedy = speedY;
	//					g_enemybullet[i].angle = atan2(e_player->position.y - e_enemy[j]->position.y, e_player->position.x - e_enemy[j]->position.x);
	//					g_enemybullet[i].isUseBullet = true;
	//					break;
	//				}
	//				else if (e_enemy[j]->enemyType == MEDIUM)
	//				{
	//					g_enemybullet[i].bulletcollision.centerposition = g_enemybullet[j].position;
	//					g_enemybullet[i].position.x = x;
	//					g_enemybullet[i].position.y = y;
	//					g_enemybullet[i].bulletSpeedx = speedX;
	//					g_enemybullet[i].bulletSpeedy = speedY;
	//					g_enemybullet[i].isUseBullet = true;
	//					break;
	//				}
	//			}
	//		}
	//	}
	//}
}

CIRCLE GetEnemyBulletCollision(int index)
{
	return g_enemybullet[index].bulletcollision;
}

E_BULLET* GetEnemyBullet()
{
	return g_enemybullet;
}