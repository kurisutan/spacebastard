#include "enemy.h"
#include "player.h"
#include "stage_one.h"
#include "sprite.h"
#include "texture.h"

static ENEMY *pEnemy;
static PLAYER *s_Player = GetPlayer();
static STAGEONE g_stageone;

int stageoneTimer;

void InitStageone()
{
	pEnemy = GetEnemy();
}
void UninitStageone()
{

}
void UpdateStageone()
{
	stageoneTimer++;

	if (stageoneTimer >= 60)
	{
		g_stageone.stageoneTimer++;
		stageoneTimer = 0;
	}
	if (stageoneTimer > 0)
	{
		for (int i = 0; i < MAXENEMY; i++)
		{
			if ((pEnemy + i)->isUseEnemy && (pEnemy + i)->enemyType == SMALL)
			{
				(pEnemy + i)->position.x -= 1;
			}
			else if ((pEnemy + i)->isUseEnemy && (pEnemy + i)->enemyType == MEDIUM)
			{
				(pEnemy + i)->position.x -= 2;
			}
		}
	}
}
void DrawStageone()
{

}

STAGEONE *GetStageone()
{
	return &g_stageone;
}