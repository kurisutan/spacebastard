//インクルードガード
#ifndef _MYDIRECT3D_H_
#define _MYDIRECT3D_H_

#include"common.h"
/***************************************
			プロトタイプ宣言
****************************************/
bool MYDirect3D_Init(HWND hwnd);//初期化
void MYDirect3D_Uninit(void);	//終了処理
LPDIRECT3DDEVICE9 MYDirect3D_GetDevice(void);	//デバイスの情報をもらう

//インクルードガード
#endif
