#include "bullet.h"
#include "player.h"
#include "texture.h"
#include "sprite.h"
#include "collision.h"
#include "SpriteAnim.h"
#include "enemy.h"
#include "explosion.h"
#include "items.h"
#include "input.h"


static ENEMY* g_enemy;
static ITEM g_item[MAXITEM];
static bool useExplosion;
int Texture_IDItemGun = 0;				//テクスチャID
int Texture_IDItemSpecialWeapon = 0;				//テクスチャID

void itemInit()
{
	useExplosion = false;
	g_enemy = GetEnemy();
	for (int i = 0; i < MAXITEM; i++)
	{
		g_item[i].position.x = 0;
		g_item[i].position.y = 0;
		g_item[i].itemTimer = 0;
		g_item[i].collision.centerposition = (g_item + i)->position;
		g_item[i].collision.radius = 30;
		g_item[i].isUseItem = false;
		g_item[i].isUseItem1 = false;
		g_item[i].isUseItem2 = false;
		g_item[i].isUseItem3 = false;
		g_item[i].itemType = NONE;
		g_item[i].itemNumber = 0;
	}

	Texture_IDItemGun = Texture_SetLoadFile("ItemGun.png", 60, 60);				//読み込みの準備
	Texture_IDItemSpecialWeapon = Texture_SetLoadFile("ItemSpecialWeapon.png", 60, 60);				//読み込みの準備
	Texture_Load();

}

void itemUninit()
{
	Texture_Release();
}

void itemUpdate()
{
	for (int i = 0; i < MAXITEM; i++)
	{
		if (g_item[i].isUseItem)
		{
			g_item[i].itemTimer++;
			g_item[i].collision.centerposition = (g_item + i)->position;
			g_item[i].position.x -= 4;
			if (g_item[i].itemTimer > 180)
			{
				g_item[i].isUseItem = false;
				g_item[i].isUseItem1 = false;
				g_item[i].isUseItem2 = false;
				g_item[i].itemTimer = 0;
				g_item[i].position.x = 0;
			}
		}
	}
}

void itemDrow()
{
	for (int i = 0; i < MAXEXPLOSION; i++)
	{
		if (g_item[i].isUseItem)
		{
			if (g_item[i].isUseItem1 == true)
			{
				Sprite_Draw_Rotation(Texture_IDItemGun, g_item[i].position.x, g_item[i].position.y, 60, 60, 60, 60, g_item[i].position.x, g_item[i].position.y, 0);
			}
			if (g_item[i].isUseItem2 == true)
			{
				Sprite_Draw_Rotation(Texture_IDItemSpecialWeapon, g_item[i].position.x, g_item[i].position.y, 60, 60, 60, 60, g_item[i].position.x, g_item[i].position.y, 0);
			}
		}
	}
}

void CreateItem(int type, float x, float y)
{
	for (int i = 0; i < MAXEXPLOSION; i++)
	{
		if (!g_item[i].isUseItem)
		{
			g_item[i].collision.centerposition = (g_item + i)->position;
			g_item[i].position.x = x;
			g_item[i].position.y = y;
			g_item[i].isUseItem = true;
			g_item[i].itemNumber = type;
			if (g_item[i].itemNumber == 1)
			{
				g_item[i].isUseItem1 = true;
			}
			else if (g_item[i].itemNumber == 2)
			{
				g_item[i].isUseItem2 = true;
			}
			break;
		}
	}
}
	
CIRCLE GetItemCollision(int index)
{
	return g_item[index].collision;
}

ITEM* GetItem()
{
	return g_item;
}
