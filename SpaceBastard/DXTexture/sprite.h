//インクルードガード
#ifndef _SPRITE_H_
#define _SPRITE_H_

#define FVF_VERTEX2D (D3DFVF_XYZRHW|D3DFVF_DIFFUSE| D3DFVF_TEX1)

/***************************************
			プロトタイプ宣言
****************************************/
//頂点バッファ使用
void Sprite_Init(void);
//頂点バッファ開放
void Sprite_Uninit(void);

//テクスチャID,中心の座標<X>,中心の座標<Y>,一番最初のUV座標<X>,一番最初のUV座標<Y>,UVの幅,UVの高さ,回転角度
void Sprite_Draw(int textureId, float dx, float dy, float uv_x, float uv_y, float uv_w, float uv_h, int radian);
//テクスチャID,中心の座標<X>,中心の座標<Y>,カットしたいテクスチャのピクセル座標<X>,カットしたいテクスチャのピクセル座標<Y>,カットしたいテクスチャの幅(ピクセル数),カットしたいテクスチャの高さ(ピクセル数)
void Sprite_Draw_Cut(int textureId, float dx, float dy, int cut_x, int cut_y, int cut_w, int cut_h);
//テクスチャID,中心の座標<X>,中心の座標<Y>,カットしたいテクスチャのピクセル座標<X>,カットしたいテクスチャのピクセル座標<Y>,カットしたいテクスチャの幅(ピクセル数),カットしたいテクスチャの高さ(ピクセル数),回転の中心<X>,回転の中心<Y>,回転角度
void Sprite_Draw_Rotation(int textureId, float dx, float dy, int cut_x, int cut_y, int cut_w, int cut_h, float center_x, float center_y, float angle);
void Sprite_Draw_Rotate(int textureId, float dx, float dy, int cut_x, int cut_y, int cut_w, int cut_h, float center_x, float center_y, float angle);
//テクスチャID,中心の座標<X>,中心の座標<Y>,一番最初のUV座標<X>,一番最初のUV座標<Y>,UVの幅,UVの高さ,横の拡大,縦の拡大,奥行きの拡大
void Sprite_Drow_Expansion(int textureId, float dx, float dy, float uv_x, float uv_y, float uv_w, float uv_h, float Xscale, float Yscale, float Zscale);

void Sprite_Draw(int textureId, float dx, float dy, int cut_x, int cut_y, int cut_w, int cut_h);


//THESE ARE FOR ROTATION
float RotateX(float x, float y, float rotate);
float RotateY(float x, float y, float rotate);
float CenterRotateX(float centerx, float centery, float x, float y, float rotate);
float CenterRotateY(float centerx, float centery, float x, float y, float rotate);

float cameraX(float objectx);
float cameraY(float objecty);


/****************************************
				構造体
*****************************************/
struct Vertex2d
{
	D3DXVECTOR4 position;					//座標  D3DX==d3dx9
	D3DCOLOR color;							//色を管理(座標の下に書く)
	FLOAT tu, tv;							//UV
};


//インクルードガード
#endif

/******************************************
<スプライトの回転>
行列(マトリックス)の利用====ベクトル(原点座標)座標変換(回転、拡大、縮小、平行移動)
*******************************************/