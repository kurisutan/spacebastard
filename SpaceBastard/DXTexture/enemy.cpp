#include "bullet.h"
#include "enemybullet.h"
#include "player.h"
#include "texture.h"
#include "sprite.h"
#include "SpriteAnim.h"
#include "enemy.h"
#include "collision.h"

ENEMY g_enemy[MAXENEMY];
PLAYER *e_Player;

static bool useEnemy;
int Texture_IDenemySmall = 0;				//テクスチャID
int Texture_IDenemyMedium = 0;				//テクスチャID
int Texture_IDenemyLarge = 0;				//テクスチャID
D3DXVECTOR2 g_enemyposition;
static float enemyMakerTimer;

void EnemyInit()
{
	useEnemy = false;
	for (int i = 0; i < MAXENEMY; i++)
	{
		g_enemy[i].isUseEnemy = false;
		g_enemy[i].position.x = 1980;
		g_enemy[i].position.y = 0;
		g_enemy[i].collision.centerposition = g_enemy->position;
		g_enemy[i].collision.radius = 0;
		g_enemy[i].basicenemyHP = 2;
		g_enemy[i].enemyCurveX = 0;
		g_enemy[i].enemyCurveY = 0;
		g_enemy[i].enemySpeed = 0;
		g_enemy[i].enemyType = SMALL;
		g_enemy[i].enemyTimer = 120;
	}
	enemyMakerTimer = 0;
	Texture_IDenemySmall = Texture_SetLoadFile("enemy-small.png", ENEMYS_W * 2, ENEMYS_H * 2);				//読み込みの準備
	Texture_IDenemyMedium = Texture_SetLoadFile("enemy-medium.png", ENEMYM_W * 2, ENEMYM_H * 2);				//読み込みの準備
	Texture_IDenemyLarge = Texture_SetLoadFile("enemy-big.png", ENEMYL_W * 2, ENEMYL_H * 2);				//読み込みの準備
	Texture_Load();
}
void EnemyUnnit()
{
	Texture_Release();					//テクスチャデータ開放
}
void EnemyUpdate()
{
	for (int i = 0; i < MAXENEMY; i++)
	{
		if (g_enemy[i].isUseEnemy)
		{

			if (g_enemy[i].enemyTimer >= 120 && g_enemy[i].enemyType == SMALL)
			{
				g_enemy[i].enemyTimer = 0;
				CreateEnemyBullet(ENORMAL, g_enemy[i].position.x, g_enemy[i].position.y,-5,0);
				break;
			}
			else if ((g_enemy[i].enemyTimer >= 120 && g_enemy[i].enemyType == MEDIUM))
			{
 				g_enemy[i].enemyTimer = 0;
  				CreateEnemyBullet(TRIPPLE, g_enemy[i].position.x, g_enemy[i].position.y, -5, 2);
				CreateEnemyBullet(TRIPPLE, g_enemy[i].position.x, g_enemy[i].position.y, -5, 0);
				CreateEnemyBullet(TRIPPLE, g_enemy[i].position.x, g_enemy[i].position.y, -5, -2);
				break;
			}


			//Changing Hit box depending on Enemy Size//
			g_enemy[i].collision.centerposition = g_enemy[i].position;

			if (g_enemy[i].enemyType == SMALL)
			{
				g_enemy[i].collision.radius = ENEMYS_W / 2;
			}
			else if (g_enemy[i].enemyType == MEDIUM)
			{
				g_enemy[i].collision.radius = ENEMYM_W / 2;
			}
			else if (g_enemy[i].enemyType == LARGE)
			{
				g_enemy[i].collision.radius = ENEMYL_W / 2;
			}


			//This is an example code for curving enemy movement//
			//g_enemy[i].enemyCurveX += 0.02f;
			//g_enemy[i].enemyCurveY += 0.03f;
			//g_enemy[i].enemySpeed -= 2;

			////Enemy Curving upwards from spawn
			//g_enemy[i].position.x = cos(g_enemy[i].enemyCurveX) * 300 + 1980;
			//g_enemy[i].position.y = sin(g_enemy[i].enemyCurveY) * 300 + 400;

			//This is used for Enemy Reset//
			if (g_enemy[i].position.x < 0)
			{
				void resetEnemy();

			}
			else if (g_enemy[i].position.y < 0)
			{
				void resetEnemy();
			}

			g_enemy[i].enemyTimer++;

			//else if (g_enemy[i].position.y > 720 || g_enemy[i].position.y < 0)
			//{
			//	g_enemy[i].isUseEnemy = false;
			//}
		}
	}
}
void EnemyDrow()
{
	e_Player = GetPlayer();
	for (int i = 0; i < MAXENEMY; i++)
	{
		if (g_enemy[i].isUseEnemy)
		{
			if (g_enemy[i].enemyType == SMALL)
			{
				SpriteAnim_DrowFlip(Texture_IDenemySmall, cameraX(g_enemy[i].position.x), cameraY(g_enemy[i].position.y), ENEMYS_W, ENEMYS_H * 2, ENEMYS_W * 2, ENEMYS_H * 2);
			}
			else if (g_enemy[i].enemyType == MEDIUM)
			{
				SpriteAnim_DrowFlip(Texture_IDenemyMedium, cameraX(g_enemy[i].position.x), cameraY(g_enemy[i].position.y), ENEMYM_W, ENEMYM_H * 2, ENEMYM_W * 2, ENEMYM_H * 2);
			}
			else if (g_enemy[i].enemyType == LARGE)
			{
				SpriteAnim_DrowFlip(Texture_IDenemyMedium, cameraX(g_enemy[i].position.x), cameraY(g_enemy[i].position.y), ENEMYL_W, ENEMYL_H * 2, ENEMYL_W * 2, ENEMYL_H * 2);
			}
		}
	}
}
void CreateEnemy(int type, float x, float y)
{
	if (enemyMakerTimer >= 120)
	{
		for (int i = 0; i < MAXENEMY; i++)
		{
			if (!g_enemy[i].isUseEnemy)
			{
				g_enemy[i].collision.centerposition = g_enemy[i].position;
				g_enemy[i].position.x = x;
				g_enemy[i].position.y = y;
				g_enemy[i].isUseEnemy = true;
				if (type == 0)
				{
					g_enemy[i].enemyType = SMALL;
					g_enemy[i].basicenemyHP = 3;
				}
				if (type == 1)
				{
					g_enemy[i].enemyType = MEDIUM;
					g_enemy[i].basicenemyHP = 4;
				}
				if (type == 2)
				{
					g_enemy[i].enemyType = MEDIUM;
					g_enemy[i].basicenemyHP = 6;
				}
				break;
			}
		}
		enemyMakerTimer = 0;
	}
	enemyMakerTimer++;
}

void resetEnemy()
{
	for (int i = 0; i < MAXENEMY; i++)
	{
		if (g_enemy[i].isUseEnemy)
		{
			g_enemy[i].isUseEnemy = false;
			g_enemy[i].enemyCurveX = 0;
			g_enemy[i].enemyCurveY = 0;
			g_enemy[i].enemySpeed = 0;
			g_enemy[i].enemyTimer = 0;
		}
	}
}

void enemyShoot()
{
	for (int i = 0; i < MAXENEMY; i++)
	{
		if (g_enemy[i].isUseEnemy)
		{

		}
	}
}

CIRCLE GetEnemyCollision(int index)
{
	return g_enemy[index].collision;
}

ENEMY* GetEnemy()
{
	return g_enemy;
}