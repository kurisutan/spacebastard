/***************************************************************************************************************************

＜ゲーム処理＞
2019年05月15日（水曜日）　古屋　亮
***************************************************************************************************************************/

/****************************************
				ヘッダー読み込み
*****************************************/
#include	"main.h"
#include	"Direct3D.h"
#include	 "debug_font.h"
#include	 "system_timer.h"
#include	"input.h"
#include	"game.h"
#include	"bullet.h"
#include	"fade.h"
#include	"player.h"
#include	"enemy.h"
#include	"enemybullet.h"
#include	"sprite.h"
/**************************************
			　グローバル変数
**************************************/
static LPDIRECT3DDEVICE9 My_Device = NULL;		//デバイス情報を受け取る変数
static PLAYER *m_Player;
static BULLET *m_Bullet = GetBullet();
static ENEMY *m_Enemy;
static E_BULLET *m_EnemyBullet;

static int g_FrameCount = 0;					//フレームカウンター
static int g_FPSBaseFrameCount = 0;				//FPS計測用フレームカウンター
static double g_FPSBaseTime = 0.0;				//FPS計測用時間
static float g_FPS = 0.0f;						//FPS
static float g_StaticFrameTime = 0.0;			//FPS制御

/**************************************
				メイン
**************************************/
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	WNDCLASS wc = {};				//ウィンドウズ構造体｛0で初期化｝
	wc.lpfnWndProc = WndProc;		//ウィンドウプロシージャーを入れる変数。＜機能＞ウィンドウからのメッセージを受け取る。関数名だけになると、そのアドレス値になる
	wc.lpszClassName = CLASS_NAME;	//クラス呼び出し
	wc.hInstance = hInstance;		//IPアドレス受け取る
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);	//カーソルの表示
	wc.hbrBackground = (HBRUSH)(COLOR_BACKGROUND + 0);	//バックグランドの色
	RegisterClass(&wc);				//登録するための関数（＆引数）


	//RECT(矩形)
	RECT window_rect = { 0,0,SCREHN_WIDTH ,SCREHN_HEIGHT };
	//画面サイズを調整してくれる
	AdjustWindowRect(&window_rect, WINDOW_STYLE ^ (WS_THICKFRAME | WS_MAXIMIZEBOX), false);
	int Window_width = window_rect.right - window_rect.left;	//ウィンドウの幅をもらう
	int Window_height = window_rect.bottom - window_rect.top;	//ウィンドウの高さをもらう


	//画面の中心に表示
	int desktop_width = GetSystemMetrics(SM_CXSCREEN);	//プライマリモニターの幅をもらう
	int desktop_height = GetSystemMetrics(SM_CYSCREEN);	//プライマリモニターの高さをもらう
	int wimdow_x = max((desktop_width - Window_width) / 2, 0);	//＜全体-物体/2＞で中心に表示できる(横) <max>で大きい方を返す
	int wimdow_y = max((desktop_height - Window_height) / 2, 0);	//＜全体-物体/2＞で中心に表示できる(高さ) <max>で大きい方を返す


	HWND hwnd = CreateWindow(		//ウィンドウを作る（オリジナル）指定する幅高さはスクリーンサイズはほかの部分足した長さで指定する
		CLASS_NAME,					//クラス呼び出し
		WINDOW_CAPTON,				//ウィンドウの見出しみたいなやつ！！！！
		WINDOW_STYLE ^ (WS_THICKFRAME | WS_MAXIMIZEBOX),		//ウィンドウの形の構成（ビット演算）< ^ >はXOR
		wimdow_x,					//X
		wimdow_y,					//Y
		Window_width,				//幅
		Window_height,				//高さ
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ShowWindow(hwnd, nCmdShow);		//ウィンドウの表示
	UpdateWindow(hwnd);				//ウィンドウの更新

	MSG msg = {};

	Keyboard_Initialize(hInstance, hwnd);	//キーボードの初期化

	Init(hwnd);
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else				//ゲーム処理
		{
			double time = SystemTimer_GetTime();	//現在時刻記憶
			if (time - g_StaticFrameTime < 1.0 / 60.0)	//1秒間に60FPSより低かったら
			{
				Sleep(0);	//一休み
			}
			else
			{
				g_StaticFrameTime = time;			//更新時間を記憶

				Update();

				Draw();
				
			}
		}
	}
	Uninit();
	return(int)msg.wParam;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)	//ここで操作できる
	{
	case WM_KEYDOWN:	//ボタンがおされたら
		if (wParam == VK_ESCAPE)
		{
			SendMessage(hWnd, WM_CLOSE, 0, 0);		//自分でメッセージをハックできる。
		}
		break;

	case WM_CLOSE:		//ウィンドウを閉じる
		if (MessageBox(hWnd, "本当に終了してよろしいですか？", "確認", MB_OKCANCEL | MB_DEFBUTTON2) == IDOK)
		{
			DestroyWindow(hWnd);
		}
		return 0;

	case WM_DESTROY:	//ウィンドウを破壊する
		PostQuitMessage(0);
		return 0;
	};
	return DefWindowProc(hWnd, uMsg, wParam, lParam);	//普通の動きができる、終了処理は必ず自分でする
}


/************************************************************
*						初期化								*
*************************************************************/
void Init(HWND hWnd)
{
	MYDirect3D_Init(hWnd);		//デバイスの初期化
	My_Device = MYDirect3D_GetDevice();//デバイス情報を受け取る

	DebugFont_Initialize();		//デバックフォントの初期化
	SystemTimer_Initialize();	//システムタイマーの初期化
	SystemTimer_Start();		//システムタイマー起動
	g_FrameCount = g_FPSBaseFrameCount = 0;
	g_FPSBaseTime = SystemTimer_GetTime();//現在の時間格納
	g_FPS = 0.0f;
	g_StaticFrameTime = 0.0;			//FPS制御

	gameInit();					//ゲーム処理初期化
}


/************************************************************
*						終了処理							*
*************************************************************/
void Uninit(void)
{
	gameUnnit();				//ゲーム処理終了処理
	Keyboard_Finalize();		//キーボード終了処理
	DebugFont_Finalize();		//フォントデバイス開放
	MYDirect3D_Uninit();		//Direct3Dのデバイス開放
}

/************************************************************
*							更新						    *
*************************************************************/
void Update(void)
{
	Keyboard_Update();	//キーボード更新

	gameUpdate();		//ゲーム処理更新
	UpdateFade();

	g_FrameCount++;	//フレームカウント増加
	double time = SystemTimer_GetTime();
	if (time - g_FPSBaseTime >= FPS_MEASUREMENT_TIME)	//計測間隔(現在の時間-前回測定した時間)
	{
		g_FPS = (float)((g_FrameCount - g_FPSBaseFrameCount) / (time - g_FPSBaseTime));		//FPSの計測(<今のFPS-前回のFPS>/<今の時間-前の時間>)
		g_FPSBaseTime = time;																//今回の時間を記憶
		g_FPSBaseFrameCount = g_FrameCount;													//今回のFPSを記憶
	}
}

/************************************************************
*							描画							*
*************************************************************/
void Draw(void)
{
	My_Device->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_RGBA(255, 255, 255, 1), 1.0f, 0.0f);		//BeginScene,EndSceneの中には書かない

	My_Device->BeginScene();	//EndSceneとセット

	gameDrow();					//ゲーム処理描画

	DebugFont_Draw(32, 32, "%.1f", g_FPS);	//FPS表示

	m_Player = GetPlayer();
	m_Enemy = GetEnemy();
	m_EnemyBullet = GetEnemyBullet();

	DebugFont_Draw(32, 50, "%.2f", m_Player->position.x);	//FPS表示
	DebugFont_Draw(32, 70, "%.2f", m_Player->position.y);	//FPS表示
	DebugFont_Draw(32, 90, "%.2f", m_Player->facing);	//FPS表示

	DebugFont_Draw(150, 50, "%.2f", m_Player->collision.centerposition.x);	//FPS表示
	DebugFont_Draw(150, 70, "%.2f", m_Player->collision.centerposition.y);	//FPS表示

	DebugFont_Draw(300, 50, "%.2f", m_Player->maxWidth);	//FPS表示
	DebugFont_Draw(300, 70, "%.2f", m_Player->maxHeight);	//FPS表示

	My_Device->EndScene();
	My_Device->Present(NULL, NULL, NULL, NULL);	//描画処理をする

}
