#ifndef _ITEM_H_
#define _ITEM_H_

#define MAXITEM (50)

enum ITEMTYPE
{
	NONE,
	BURST,
	SAW,
	BEAM,
	MAX,
};

struct ITEM
{
	D3DXVECTOR2 position;
	CIRCLE collision;
	bool isUseItem;
	bool isUseItem1;
	bool isUseItem2;
	bool isUseItem3;

	float itemTimer;
	ITEMTYPE itemType;
	int itemNumber;
};

void itemInit(void);
void itemUninit(void);
void itemUpdate(void);
void itemDrow(void);
void CreateItem(int type, float x, float y);

CIRCLE GetItemCollision(int index);
ITEM* GetItem();


#endif _ITEM_H_

