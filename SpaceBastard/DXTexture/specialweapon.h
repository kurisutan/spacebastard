#pragma once
#include "Direct3D.h"
#include "common.h"
#include "collision.h"

enum SPWEAPONTYPE
{
	SPNONE,
	SPSAW,
	SPBEAM,
	SPMAX,
};

struct SPECIALWEAPON
{
	D3DXVECTOR2 position;
	CIRCLE spcollision;
	double radian;
	bool isUseSp;
	SPWEAPONTYPE type;
};

void spWeaponInit();
void spWeaponUninit();
void spWeaponUpdate();
void spWeaponDraw();
SPECIALWEAPON* GetSpecialWeapon();
CIRCLE GetSpCollision();