#include "mapeditor.h"
#include "Direct3D.h"
#include "sprite.h"
#include "texture.h"
#include "input.h"
#include "player.h"


bool editormode;

static MAPEDITOR g_mapEditor[MAXBLOCK];

PLAYER *m_Player = GetPlayer();

void InitMapEditor()
{

	for (int i = 0; i < MAXBLOCK; i++)
	{
		g_mapEditor[i].posx = 5;
		g_mapEditor[i].posy = 5;
		g_mapEditor[i].angle = 0;
		g_mapEditor[i].place = false;
		g_mapEditor[i].erase = false;
		g_mapEditor[i].maxBlock = 0;
		g_mapEditor[i].maxEnemy = 0;

		//For asteroid
		g_mapEditor[i].asteroid.posx = 0;
		g_mapEditor[i].asteroid.posy = 0;
		g_mapEditor[i].asteroid.angle = 0;
		g_mapEditor[i].asteroid.TextureIDAsteroid = Texture_SetLoadFile("asteroid_grey.png", 55 * 2, 55 * 2);

		//For asteroid
		g_mapEditor[i].dirtwall.posx = 0;
		g_mapEditor[i].dirtwall.posy = 0;
		g_mapEditor[i].dirtwall.angle = 0;
		g_mapEditor[i].dirtwall.TextureIDBlock = Texture_SetLoadFile("dirtblock.png", 55 * 2, 55 * 2);

	}



	Texture_Load();
	editormode = false;
	m_Player->position.x = 0;
	m_Player->position.y = 0;
}

void UninitMapEditor()
{

}

void UpdateMapEditor()
{
	for (int i = 0; i < MAXBLOCK; i++)
	{
		if (!g_mapEditor[i].place)
		{
			if (Keyboard_IsPress(DIK_W))
			{
				g_mapEditor[i].posy -= 5;
				m_Player->position.y -= g_mapEditor[i].posy;
			}
			else if (Keyboard_IsPress(DIK_S))
			{
				g_mapEditor[i].posy += 5;
				m_Player->position.y += g_mapEditor[i].posy;
			}
			else if (Keyboard_IsPress(DIK_A))
			{
				g_mapEditor[i].posx -= 5;
				m_Player->position.y -= g_mapEditor[i].posy;
			}
			else if (Keyboard_IsPress(DIK_D))
			{
				g_mapEditor[i].posx += 5;
				m_Player->position.y += g_mapEditor[i].posy;
			}
			else if (Keyboard_IsTrigger(DIK_RETURN))
			{
				g_mapEditor[i].maxBlock++;
				g_mapEditor[i].dirtwall.posx;
				g_mapEditor[i].dirtwall.posy;
				g_mapEditor[g_mapEditor[i].maxBlock].place = true;
			}
			m_Player->position.x = g_mapEditor[i].posx;
			m_Player->position.y = g_mapEditor[i].posy;
		}
	}
	if (Keyboard_IsTrigger(DIK_TAB))
	{
		editormode = true;
	}
}

void DrawMapEditor()
{
	for (int i = 0; i < MAXBLOCK; i++)
	{
		if (!g_mapEditor[i].place && !editormode)
		{
			Sprite_Draw_Rotate(g_mapEditor[i].dirtwall.TextureIDBlock, SCREHN_WIDTH / 2, SCREHN_HEIGHT / 2, 55, 55, 55, 55, 55 / 2, 55 / 2, g_mapEditor[i].angle);
		}
		
		if(g_mapEditor[i].place)
		{
			Sprite_Draw_Rotate(g_mapEditor[i].dirtwall.TextureIDBlock, cameraX(g_mapEditor[i].posx), cameraY(g_mapEditor[i].posy), 55, 55, 55, 55, 55 / 2, 55 / 2, g_mapEditor[i].angle);
		}
	}
}
