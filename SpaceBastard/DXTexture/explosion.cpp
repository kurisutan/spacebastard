#include "bullet.h"
#include "player.h"
#include "texture.h"
#include "sprite.h"
#include "collision.h"
#include "SpriteAnim.h"
#include "enemy.h"
#include "explosion.h"



static ENEMY* g_enemy;
static EXPLOSION g_explosion[MAXEXPLOSION];
static bool useExplosion;
int Texture_IDExplosion = 2;				//テクスチャID

int returnzero = 0;

void explosionInit()
{
	useExplosion = false;
	g_enemy = GetEnemy();
	for (int i = 0; i < MAXEXPLOSION; i++)
	{
		g_explosion[i].position.x = 0;
		g_explosion[i].position.y = 0;
		g_explosion[i].ExplosionTimer = 0;
		g_explosion[i].isUseExplosion = false;
	}

	Texture_IDExplosion = Texture_SetLoadFile("explosion.png", 160, 32);				//読み込みの準備
	Texture_Load();

}

void explosionUninit()
{
	Texture_Release();
}

void explosionUpdate()
{
	for (int i = 0; i < MAXEXPLOSION; i++)
	{
		if (g_explosion[i].isUseExplosion)
		{
			g_explosion[i].ExplosionTimer++;
			if (g_explosion[i].ExplosionTimer > 30)
			{
				g_explosion[i].isUseExplosion = false;
				g_explosion[i].ExplosionTimer = 0;
			}
		}
	}
}

void explosionDrow()
{
	for (int i = 0; i < MAXEXPLOSION; i++)
	{
		if (g_explosion[i].isUseExplosion)
		{
			SpriteAnim_DrowExplosion(Texture_IDExplosion,g_explosion[i].position.x, g_explosion[i].position.y, 32, 32);
		}
	}
	//SpriteAnim_DrowExplosion(500, 500, 32, 32);
}

void CreateExplosion(float x, float y)
{
	for (int i = 0; i < MAXEXPLOSION; i++)
	{
		if (!g_explosion[i].isUseExplosion)
		{
			g_explosion[i].position.x = x;
			g_explosion[i].position.y = y;
			g_explosion[i].isUseExplosion = true;
			break;
		}
	}
}

int GetFrameCounterZero()
{
	return returnzero;
}
